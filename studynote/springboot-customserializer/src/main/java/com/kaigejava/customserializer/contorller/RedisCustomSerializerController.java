package com.kaigejava.customserializer.contorller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/3/28 23:26
 */
@RestController
public class RedisCustomSerializerController {
    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Resource
    private ReactiveRedisTemplate<String, Object> reactiveRedisTemplate;


    @GetMapping("/test")
    public String test(String msg){
        String key = "history:2022011307:044078f7440c641a0291_20021";
        redisTemplate.opsForValue().set(key,"价格dd:"+msg);
        return "OK";
    }

    @GetMapping("/get-test")
    public String getTest(String key){
        System.out.println("key="+key);
       /* Object obj =  reactiveRedisTemplate.opsForValue().get(key).subscribe(
                device -> show(device)
        );*/
        Object obj =  reactiveRedisTemplate.opsForValue().get(key).block();
        //JSONObject jsonObject = JSON.parseObject(device.toString());
        System.out.println("value:"+obj);
        return "value="+obj;
    }

    private void show(Object device) {
        System.out.println("获取到的device:"+device);
    }

    /**
     *
     *   private void setCache(ReportPropertyMessage message, String key, Long timestamptag, Object device) {
     *         Date date = new Date();
     *         JSONObject jsonObject = JSON.parseObject(device.toString());
     *         //赋值
     *         message.getProperties().forEach((k, v) -> {
     *             if ("Ep".equals(k) && (Double.parseDouble(v.toString()) > jsonObject.getDouble(epMax))) {
     *                 jsonObject.put(epMax, v);
     *             }
     *             if (keyList.stream().anyMatch(iotKey -> iotKey.equals(k))) {
     *                 jsonObject.put(k + "Num", jsonObject.getInteger(k + "Num") + 1);
     *                 //默认值替换为参数
     *                 jsonObject.put(k, (jsonObject.getDouble(k) + Double.parseDouble(v.toString())));
     *             }
     *         });
     *         //采集时间
     *         String string = timestamptag.toString();
     *         date.setTime(Long.parseLong(string));
     *         jsonObject.put(collectTime, dateFormat.format(date));
     *         addredis(jsonObject, key);
     *     }
     *
     *
     *   public void addredis(JSONObject jsonObject, String key) {
     *         reactiveRedisTemplate.
     *             opsForValue()
     *             .set(key, jsonObject)
     *             .subscribe();
     *         //释放锁
     *         reactiveRedisTemplate.opsForValue().delete("Synchronized:" + key).subscribe();
     *     }

     */
    @GetMapping("/get-test-all")
    public String getTestAll(String keyPre){
        System.out.println("key="+keyPre);
        Set keys = redisTemplate.keys(keyPre + ":*");
        if (ObjectUtil.isNotEmpty(keys)) {
           // redisTemplate.opsForHash().
        }
        return "ddd";
    }
}
