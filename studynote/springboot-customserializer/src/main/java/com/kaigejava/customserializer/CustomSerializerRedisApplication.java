package com.kaigejava.customserializer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author 凯哥Java
 * @description Redis启动类
 * @company
 * @since 2022/11/1 22:15
 */
@SpringBootApplication
@EnableCaching
public class CustomSerializerRedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomSerializerRedisApplication.class,args);
        System.out.println("======================== Redis-customserializer 项目启动完成! ========================");
    }
}
