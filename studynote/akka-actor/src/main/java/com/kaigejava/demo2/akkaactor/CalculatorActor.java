package com.kaigejava.demo2.akkaactor;
import akka.actor.AbstractActor;
import com.kaigejava.demo2.messagedto.CalculateMessageDTO;
import com.kaigejava.demo2.messagedto.PrintMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo2.akkaactor
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:36
 * @Description: 用于计算的
 * @Version: 1.0
 */
public class CalculatorActor  extends AbstractActor {

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CalculateMessageDTO.class, this::calculate)
                .build();
    }

    private void calculate(CalculateMessageDTO message) {
        int result = message.getA() + message.getB();
        getContext().getSystem().actorSelection("/user/printerActor").tell(new PrintMessageDTO(result), getSelf());
    }

}
