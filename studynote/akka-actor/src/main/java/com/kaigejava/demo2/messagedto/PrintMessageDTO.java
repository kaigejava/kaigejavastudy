package com.kaigejava.demo2.messagedto;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo2.messagedto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:40
 * @Description: 用于打印的消息对象
 * @Version: 1.0
 */
public class PrintMessageDTO {
    private final int result;

    public PrintMessageDTO(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }
}
