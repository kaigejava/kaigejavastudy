package com.kaigejava.demo2;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.kaigejava.demo2.akkaactor.CalculatorActor;
import com.kaigejava.demo2.akkaactor.PrinterActor;
import com.kaigejava.demo2.messagedto.CalculateMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo2
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:41
 * @Description: 测试类
 * @Version: 1.0
 */
public class MainB {

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("system");

        ActorRef calculatorActor = system.actorOf(Props.create(CalculatorActor.class), "calculatorActor");
        ActorRef printerActor = system.actorOf(Props.create(PrinterActor.class), "printerActor");

        calculatorActor.tell(new CalculateMessageDTO(15, 10), ActorRef.noSender());
    }
}
