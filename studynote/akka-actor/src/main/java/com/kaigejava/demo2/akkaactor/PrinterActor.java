package com.kaigejava.demo2.akkaactor;

import akka.actor.AbstractActor;
import com.kaigejava.demo2.messagedto.PrintMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo2.akkaactor
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:37
 * @Description: 用来输出的
 * @Version: 1.0
 */
public class PrinterActor   extends AbstractActor {


        @Override
        public Receive createReceive() {
            return receiveBuilder()
                    .match(PrintMessageDTO.class, this::print)
                    .build();
        }

        private void print(PrintMessageDTO message){
            System.out.println(message.getResult());
        }

}

