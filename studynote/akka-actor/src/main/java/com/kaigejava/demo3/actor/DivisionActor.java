package com.kaigejava.demo3.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.kaigejava.demo2.messagedto.PrintMessageDTO;
import com.kaigejava.demo3.RuleMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3.actor
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:14
 * @Description: TODO
 * @Version: 1.0
 */
public class DivisionActor extends AbstractActor {

    private ActorRef printerActor;

    public DivisionActor() {
        printerActor = getContext().getSystem().actorOf(Props.create(PrinterActor.class), "printerActor");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RuleMessageDTO.class, this::divide)
                .build();
    }

    private void divide(RuleMessageDTO message) {
        int result = message.getResult() / 5;
        printerActor.tell(new PrintMessageDTO(result), getSelf());
    }
}
