package com.kaigejava.demo3;

import java.util.List;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:16
 * @Description: TODO
 * @Version: 1.0
 */
public class SetRulesMessage {
    private List<Rule> rules;

    public SetRulesMessage(List<Rule> rules) {
        this.rules = rules;
    }

    public List<Rule> getRules() {
        return rules;
    }
}
