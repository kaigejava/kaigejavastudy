package com.kaigejava.demo3.message;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3.message
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:48
 * @Description: TODO
 * @Version: 1.0
 */
public class AddMessage {
    public final int number1;
    public final int number2;

    public AddMessage(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }
}
