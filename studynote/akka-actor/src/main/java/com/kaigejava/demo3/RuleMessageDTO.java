package com.kaigejava.demo3;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:16
 * @Description: TODO
 * @Version: 1.0
 */
public class RuleMessageDTO {
    private Rule rule;
    private int a;
    private int b;
    private int result;

    public RuleMessageDTO(Rule rule, int a, int b, int result) {
        this.rule = rule;
        this.a = a;
        this.b = b;
        this.result = result;
    }

    public Rule getRule() {
        return rule;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getResult() {
        return result;
    }
}
