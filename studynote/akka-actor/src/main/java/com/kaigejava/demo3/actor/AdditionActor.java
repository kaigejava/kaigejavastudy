package com.kaigejava.demo3.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.kaigejava.demo2.messagedto.PrintMessageDTO;
import com.kaigejava.demo3.Rule;
import com.kaigejava.demo3.RuleMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3.actor
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:13
 * @Description: TODO
 * @Version: 1.0
 */
public class AdditionActor  extends AbstractActor {

    private ActorRef printerActor;

    public AdditionActor() {
        printerActor = getContext().getSystem().actorOf(Props.create(PrinterActor.class), "additionPrinterActor");

    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RuleMessageDTO.class, this::add)
                .build();
    }

    private void add(RuleMessageDTO message) {
        int result = message.getA() + message.getB() + message.getResult();
        Rule rule = message.getRule();
        if (result < rule.getStart()) {
            printerActor.tell(new PrintMessageDTO(result), getSelf());
        } else {
            ActorRef targetActor = rule.getTargetActor();
            targetActor.tell(new RuleMessageDTO(rule, message.getA(), message.getB(), result), getSelf());
        }
    }
}
