package com.kaigejava.demo3;

import akka.actor.ActorRef;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:15
 * @Description: TODO
 * @Version: 1.0
 */
public class Rule {
    private int start;
    private int end;
    private ActorRef targetActor;

    public Rule(int start, int end, ActorRef targetActor) {
        this.start = start;
        this.end = end;
        this.targetActor = targetActor;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public ActorRef getTargetActor() {
        return targetActor;
    }
}
