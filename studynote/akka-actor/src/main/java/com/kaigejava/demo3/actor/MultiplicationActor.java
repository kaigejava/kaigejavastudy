package com.kaigejava.demo3.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.kaigejava.demo3.Rule;
import com.kaigejava.demo3.RuleMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3.actor
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:14
 * @Description: TODO
 * @Version: 1.0
 */
public class MultiplicationActor extends AbstractActor {

    private ActorRef divisionActor;

    public MultiplicationActor() {
       // divisionActor = getContext().getSystem().actorOf(Props.create(DivisionActor.class), "divisionActor");
        divisionActor = getContext().getSystem().actorOf(Props.create(DivisionActor.class), "multiplicationDivisionActor");

    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(RuleMessageDTO.class, this::multiply)
                .build();
    }

    private void multiply(RuleMessageDTO message) {
        int result = message.getResult() * 100;
        Rule rule = message.getRule();
        ActorRef targetActor = rule.getTargetActor();
        targetActor.tell(new RuleMessageDTO(rule, message.getA(), message.getB(), result), getSelf());
    }
}
