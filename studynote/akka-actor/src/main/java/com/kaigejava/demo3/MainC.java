package com.kaigejava.demo3;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:49
 * @Description: TODO
 * @Version: 1.0
 */

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.kaigejava.demo2.akkaactor.CalculatorActor;
import com.kaigejava.demo2.messagedto.CalculateMessageDTO;
import com.kaigejava.demo3.actor.AdditionActor;
import com.kaigejava.demo3.actor.MultiplicationActor;
import com.kaigejava.demo3.actor.SubtractionActor;

import java.util.ArrayList;
import java.util.List;

public class MainC {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("system");

        ActorRef calculatorActor = system.actorOf(Props.create(CalculatorActor.class), "calculatorActor");

        List<Rule> rules = new ArrayList<>();
//        rules.add(new Rule(0, 9, calculatorActor)); // If result < 10, send to calculatorActor
        rules.add(new Rule(10, 19, system.actorOf(Props.create(AdditionActor.class)))); // If 10 <= result <= 19, send to AdditionActor
//        rules.add(new Rule(20, 29, system.actorOf(Props.create(SubtractionActor.class)))); // If 20 <= result <= 29, send to SubtractionActor
//        rules.add(new Rule(30, Integer.MAX_VALUE, system.actorOf(Props.create(MultiplicationActor.class)))); // If result >= 30, send to MultiplicationActor

        calculatorActor.tell(new SetRulesMessage(rules), ActorRef.noSender());

        calculatorActor.tell(new CalculateMessageDTO(5, 10), ActorRef.noSender());
    }
}
