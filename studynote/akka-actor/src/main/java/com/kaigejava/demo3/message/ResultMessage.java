package com.kaigejava.demo3.message;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo3.message
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:48
 * @Description: TODO
 * @Version: 1.0
 */
public class ResultMessage {
    public final int sum;

    public ResultMessage(int sum) {
        this.sum = sum;
    }
}
