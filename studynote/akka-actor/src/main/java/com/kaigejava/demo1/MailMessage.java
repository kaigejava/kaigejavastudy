package com.kaigejava.demo1;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/6 17:36
 */
public class MailMessage {
    private final String content;
    private final Actor receiver;

    public MailMessage(String content, Actor receiver) {
        this.content = content;
        this.receiver = receiver;
    }

    public String getContent() {
        return content;
    }

    public Actor getReceiver() {
        return receiver;
    }
}
