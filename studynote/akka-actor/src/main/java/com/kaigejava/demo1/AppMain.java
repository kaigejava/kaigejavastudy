package com.kaigejava.demo1;

/**
 * Hello world!
 *
 */
public class AppMain
{
    public static void main(String[] args) {
        Actor alice = new Actor();
        Actor bob = new Actor();

        // Bob 收到来自 Alice 的问候消息
        alice.tell(new MailMessage("Bob", bob));

        // Alice 收到来自 Bob 的问候消息
        bob.tell(new MailMessage("Alice", alice));

        new Thread(() -> alice.act()).start();
        new Thread(() -> bob.act()).start();
    }
}
