package com.kaigejava.demo1;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/5/6 17:35
 */
public class Actor {
    private ConcurrentLinkedQueue<MailMessage> mailbox = new ConcurrentLinkedQueue<>();

    public void tell(MailMessage message) {
        mailbox.offer(message);
    }

    public void act() {
        for (int i = 0; i <10 ; i++) {
            MailMessage message = mailbox.poll();
            if (message != null) {
                System.out.println("当前线程名称为:"+Thread.currentThread().getName() + " 接受到消息为: " + message.getContent());
                message.getReceiver().tell(new MailMessage("Hello " + message.getContent() + "!", this));
            }
        }
    }
}
