package com.kaigejava.demo4.messagedto;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo2.messagedto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:38
 * @Description: 用于计算的消息对象
 * @Version: 1.0
 */
public class CalculateMessageDTO {
    private final int a;
    private final int b;

    public CalculateMessageDTO(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
