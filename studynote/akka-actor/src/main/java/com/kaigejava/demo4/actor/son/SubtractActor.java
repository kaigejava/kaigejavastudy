package com.kaigejava.demo4.actor.son;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.kaigejava.demo4.messagedto.SubtractMessageDTO;
import com.kaigejava.demo4.messagedto.result.SubtractResultDTO;

import java.time.Duration;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.actor.son
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:50
 * @Description: TODO
 * @Version: 1.0
 */
public class SubtractActor extends AbstractActor {

    private ActorRef calculatorActor;

    @Override
    public void preStart() throws Exception {
        calculatorActor = getContext().getSystem().actorSelection("/user/calculatorActor").resolveOne(Duration.ofMillis(5000)).toCompletableFuture().get();
        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SubtractMessageDTO.class, this::subtract)
                .build();
    }

    private void subtract(SubtractMessageDTO message) {
        int result = message.getResult() - message.getA() - message.getB();
        calculatorActor.tell(new SubtractResultDTO(message.getA(), message.getB(), result), getSelf());
    }
}
