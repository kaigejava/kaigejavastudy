package com.kaigejava.demo4.actor.son;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.kaigejava.demo4.messagedto.AddMessageDTO;
import com.kaigejava.demo4.messagedto.result.AddResultDTO;

import java.time.Duration;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.actor.son
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:50
 * @Description: TODO
 * @Version: 1.0
 */
public class AddActor extends AbstractActor {

    private ActorRef calculatorActor;

    @Override
    public void preStart() throws Exception {
        calculatorActor = getContext().getSystem().actorSelection("/user/calculatorActor").resolveOne(Duration.ofMillis(5000)).toCompletableFuture().get();
        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AddMessageDTO.class, this::add)
                .build();
    }

    private void add(AddMessageDTO message) {
        int result = message.getA() + message.getB();
        calculatorActor.tell(new AddResultDTO(message.getA(), message.getB(), result), getSelf());
    }
}
