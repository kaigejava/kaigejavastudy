package com.kaigejava.demo4.actor.son;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.kaigejava.demo4.messagedto.DivideMessageDTO;
import com.kaigejava.demo4.messagedto.result.DivideResultDTO;

import java.time.Duration;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.actor.son
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:51
 * @Description: TODO
 * @Version: 1.0
 */
public class DivideActor extends AbstractActor {

    private ActorRef calculatorActor;

    @Override
    public void preStart() throws Exception {
        calculatorActor = getContext().getSystem().actorSelection("/user/calculatorActor").resolveOne(Duration.ofMillis(5000)).toCompletableFuture().get();
        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(DivideMessageDTO.class, this::divide)
                .build();
    }

    private void divide(DivideMessageDTO message) {
        int result = message.getResult() / 5;
        calculatorActor.tell(new DivideResultDTO(result), getSelf());
    }
}
