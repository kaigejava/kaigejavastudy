package com.kaigejava.demo4.actor.son;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import com.kaigejava.demo4.messagedto.MultiplyMessageDTO;
import com.kaigejava.demo4.messagedto.result.MultiplyResultDTO;

import java.time.Duration;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.actor.son
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:51
 * @Description: TODO
 * @Version: 1.0
 */


public class MultiplyActor extends AbstractActor {

    private ActorRef calculatorActor;

    @Override
    public void preStart() throws Exception {
        calculatorActor = getContext().getSystem().actorSelection("/user/calculatorActor").resolveOne(Duration.ofMillis(5000)).toCompletableFuture().get();
        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MultiplyMessageDTO.class, this::multiply)
                .build();
    }

    private void multiply(MultiplyMessageDTO message) {
        int result = message.getResult() * 100;
        calculatorActor.tell(new MultiplyResultDTO(result), getSelf());
    }
}
