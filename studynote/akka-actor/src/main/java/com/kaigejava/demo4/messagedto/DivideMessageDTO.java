package com.kaigejava.demo4.messagedto;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.messagedto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:58
 * @Description: TODO
 * @Version: 1.0
 */
public class DivideMessageDTO {
    private  int dividend;
    private final int result;

    public DivideMessageDTO(int dividend, int result) {
        this.dividend = dividend;
        this.result = result;
    }

    public DivideMessageDTO( int result) {
        this.result = result;
    }

    public int getDividend() {
        return dividend;
    }

    public int getResult() {
        return result;
    }
}
