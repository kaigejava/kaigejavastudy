package com.kaigejava.demo4.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.kaigejava.demo4.actor.son.AddActor;
import com.kaigejava.demo4.actor.son.DivideActor;
import com.kaigejava.demo4.actor.son.MultiplyActor;
import com.kaigejava.demo4.actor.son.SubtractActor;
import com.kaigejava.demo4.messagedto.*;
import com.kaigejava.demo4.messagedto.result.AddResultDTO;
import com.kaigejava.demo4.messagedto.result.DivideResultDTO;
import com.kaigejava.demo4.messagedto.result.MultiplyResultDTO;
import com.kaigejava.demo4.messagedto.result.SubtractResultDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.actor
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:49
 * @Description: TODO
 * @Version: 1.0
 */
public class CalculatorActor extends AbstractActor {

    private ActorRef addActor;
    private ActorRef subtractActor;
    private ActorRef multiplyActor;
    private ActorRef divideActor;
    private ActorRef printerActor;

    @Override
    public void preStart() throws Exception {
        addActor = getContext().actorOf(Props.create(AddActor.class), "addActor");
        subtractActor = getContext().actorOf(Props.create(SubtractActor.class), "subtractActor");
        multiplyActor = getContext().actorOf(Props.create(MultiplyActor.class), "multiplyActor");
        divideActor = getContext().actorOf(Props.create(DivideActor.class), "divideActor");
        printerActor = getContext().actorOf(Props.create(PrinterActor.class), "printerActor");
        super.preStart();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CalculateMessageDTO.class, this::calculate)
                .match(AddResultDTO.class, this::handleAddResult)
                .match(SubtractResultDTO.class, this::handleSubtractResult)
                .match(MultiplyResultDTO.class, this::handleMultiplyResult)
                .match(DivideResultDTO.class, this::handleDivideResult)
                .build();
    }

    private void calculate(CalculateMessageDTO message) {
        int result = message.getA() + message.getB();

        if (result < 20) {
            addActor.tell(new AddMessageDTO(message.getA(), message.getB(), result), getSelf());
        } else if (result > 30) {
            subtractActor.tell(new SubtractMessageDTO(message.getA(), message.getB(), result), getSelf());
        } else if (result < 10) {
            printerActor.tell(new PrintMessageDTO(result), getSelf());
        } else {
            multiplyActor.tell(new MultiplyMessageDTO(result), getSelf());
        }
    }

    private void handleAddResult(AddResultDTO message) {
        subtractActor.tell(new SubtractMessageDTO(message.getA(), message.getB(), message.getResult() - 16), getSelf());
    }

    private void handleSubtractResult(SubtractResultDTO message) {
        multiplyActor.tell(new MultiplyMessageDTO(message.getResult()), getSelf());
    }

    private void handleMultiplyResult(MultiplyResultDTO message) {
        divideActor.tell(new DivideMessageDTO(message.getResult()), getSelf());
    }

    private void handleDivideResult(DivideResultDTO message) {
        printerActor.tell(new PrintMessageDTO(message.getResult()), getSelf());
    }
}
