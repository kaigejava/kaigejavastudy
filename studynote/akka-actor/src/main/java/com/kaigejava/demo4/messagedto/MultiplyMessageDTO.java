package com.kaigejava.demo4.messagedto;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.messagedto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:59
 * @Description: TODO
 * @Version: 1.0
 */
public class MultiplyMessageDTO {
    private  int factor;
    private final int result;

    public MultiplyMessageDTO(int factor, int result) {
        this.factor = factor;
        this.result = result;
    }

    public MultiplyMessageDTO( int result) {
        this.result = result;
    }

    public int getFactor() {
        return factor;
    }

    public int getResult() {
        return result;
    }
}
