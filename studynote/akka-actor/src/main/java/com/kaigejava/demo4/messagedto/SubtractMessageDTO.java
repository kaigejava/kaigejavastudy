package com.kaigejava.demo4.messagedto;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.messagedto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:59
 * @Description: TODO
 * @Version: 1.0
 */
public class SubtractMessageDTO {
    private final int a;
    private final int b;
    private final int result;

    public SubtractMessageDTO(int a, int b, int result) {
        this.a = a;
        this.b = b;
        this.result = result;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getResult() {
        return result;
    }
}
