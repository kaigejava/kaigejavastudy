package com.kaigejava.demo4.messagedto.result;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4.messagedto.result
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  16:02
 * @Description: TODO
 * @Version: 1.0
 */
public class MultiplyResultDTO {
    private final int result;

    public MultiplyResultDTO(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }
}
