package com.kaigejava.demo4;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.kaigejava.demo2.akkaactor.CalculatorActor;
import com.kaigejava.demo2.akkaactor.PrinterActor;
import com.kaigejava.demo4.messagedto.CalculateMessageDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.demo4
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  15:53
 * @Description: TODO
 * @Version: 1.0
 */
public class Main4 {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("system");

        ActorRef printerActor = system.actorOf(Props.create(PrinterActor.class), "printerActor");
        ActorRef calculatorActor = system.actorOf(Props.create(CalculatorActor.class, printerActor), "calculatorActor");

        calculatorActor.tell(new CalculateMessageDTO(5, 10), ActorRef.noSender());
    }
}
