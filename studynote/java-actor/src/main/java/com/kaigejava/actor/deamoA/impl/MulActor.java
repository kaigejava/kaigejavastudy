package com.kaigejava.actor.deamoA.impl;

import com.kaigejava.actor.deamoA.Actor;
import com.kaigejava.actor.deamoA.dto.Message;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoA.impl
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:00
 * @Description: TODO
 * @Version: 1.0
 */
public class MulActor implements Actor {
    @Override
    public void receive(Message message) {
        int result = message.getNumber1() * message.getNumber2();
        System.out.println("MulActor: " + message.getNumber1() + " * " + message.getNumber2() + " = " + result);
    }
}
