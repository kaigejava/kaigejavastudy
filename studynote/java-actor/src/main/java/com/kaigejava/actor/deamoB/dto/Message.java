package com.kaigejava.actor.deamoB.dto;

import com.kaigejava.actor.deamoB.Actor;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoB.dto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:11
 * @Description: TODO
 * @Version: 1.0
 */
public class Message {
    private final Actor sender;
    private final Object payload;

    public Message(Actor sender, Object payload) {
        this.sender = sender;
        this.payload = payload;
    }

    public Actor getSender() {
        return sender;
    }

    public Object getPayload() {
        return payload;
    }
}
