package com.kaigejava.actor.deamoA.impl;

import com.kaigejava.actor.deamoA.Actor;
import com.kaigejava.actor.deamoA.dto.Message;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoA.impl
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  13:59
 * @Description: TODO
 * @Version: 1.0
 */
public class SubActor implements Actor {
    private Actor nextActor;

    public void setNextActor(Actor nextActor) {
        this.nextActor = nextActor;
    }

    @Override
    public void receive(Message message) {
        int result = message.getNumber1() - message.getNumber2();
        System.out.println("SubActor: " + message.getNumber1() + " - " + message.getNumber2() + " = " + result);

        nextActor.receive(new Message(result, 2));   // 投递给节点C
    }
}
