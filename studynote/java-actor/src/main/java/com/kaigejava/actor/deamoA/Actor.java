package com.kaigejava.actor.deamoA;

import com.kaigejava.actor.deamoA.dto.Message;

/**
 * @Author: kaigejava
 * @Date: 2023/8/16 13:57
 * @Description:
 **/

public interface Actor {
    void receive(Message message);

}
