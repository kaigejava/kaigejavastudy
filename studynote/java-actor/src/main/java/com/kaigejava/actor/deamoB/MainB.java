package com.kaigejava.actor.deamoB;

import com.kaigejava.actor.deamoB.dto.Message;
import com.kaigejava.actor.deamoB.impl.NodeActor;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoB
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:13
 * @Description: TODO
 * @Version: 1.0
 */
public class MainB {
    public static void main(String[] args) {
        Actor node5 = new NodeActor(null, null);
        Actor node4 = new NodeActor(node5, null);
        Actor node3 = new NodeActor(null, node5);
        Actor node2 = new NodeActor(node4, node5);
        Actor node1 = new NodeActor(node2, node3);

        node1.receive(new Message(null, 5));
    }
}
