package com.kaigejava.actor.deamoA.dto;

import lombok.Data;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoA.dto
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  13:57
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class Message {
    private int number1;
    private int number2;
    private int destination;
    public Message(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }
}
