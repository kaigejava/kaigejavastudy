package com.kaigejava.actor.deamoB;

import com.kaigejava.actor.deamoB.dto.Message;

/**
 * @Author: kaigejava
 * @Date: 2023/8/16 14:11
 * @Description:
 **/

public interface Actor {
    void receive(Message message);
}
