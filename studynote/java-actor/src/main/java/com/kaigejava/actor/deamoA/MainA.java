package com.kaigejava.actor.deamoA;

import com.kaigejava.actor.deamoA.dto.Message;
import com.kaigejava.actor.deamoA.impl.AddActor;
import com.kaigejava.actor.deamoA.impl.MulActor;
import com.kaigejava.actor.deamoA.impl.SubActor;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoA
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  14:00
 * @Description: TODO
 * @Version: 1.0
 */
public class MainA {
    public static void main(String[] args) {
        AddActor addActor = new AddActor();
        SubActor subActor = new SubActor();
        MulActor mulActor = new MulActor();

        // 链接节点
        addActor.setNextActor(subActor);
        subActor.setNextActor(mulActor);

        // 构造初始消息并投递给第一个节点
        addActor.receive(new Message(5, 7));   // 结果为12，投递给节点B
        System.out.println("======================");
      //  addActor.receive(new Message(3, 4));   // 结果为7，投递给节点C
    }
}
