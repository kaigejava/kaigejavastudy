package com.kaigejava.actor.deamoA.impl;

import com.kaigejava.actor.deamoA.Actor;
import com.kaigejava.actor.deamoA.dto.Message;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.actor.deamoA.impl
 * @Author: kaigejava
 * @CreateTime: 2023-08-16  13:58
 * @Description: TODO
 * @Version: 1.0
 */
public class AddActor implements Actor {
    private Actor nextActor;

    public void setNextActor(Actor nextActor) {
        this.nextActor = nextActor;
    }

    @Override
    public void receive(Message message) {
        int result = message.getNumber1() + message.getNumber2();
        System.out.println("AddActor: " + message.getNumber1() + " + " + message.getNumber2() + " = " + result);

        if (result > 10) {
            nextActor.receive(new Message(result, 0));   // 投递给节点B
        } else {
            nextActor.receive(new Message(result, 1));   // 投递给节点C
        }
    }
}
