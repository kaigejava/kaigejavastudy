package com.kaigejava.spring.beanlife.bean;

import com.kaigejava.spring.beanlife.service.Engine;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 15:03
 */
public class BenzCar implements InitializingBean {

    public Engine engine;

    public BenzCar(){
        System.out.println("  BenzCar 无参构造器....");
        if(engine == null){
            System.out.println(" engine not setting ~~~~");
        }else{
            System.out.println(" engine 已经初始化");
        }
    }


    void startCar(){
        System.out.println(" BeanzCar start ====");
        engine.fire();
    }



    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("BenzCar afterPropertiesSet method ... ");
        if(engine == null){
            System.out.println(" afterPropertiesSet  engine not setting ~~~~");
        }else{
            System.out.println(" afterPropertiesSet engine 已经初始化");
            engine.fire();
        }
    }


    @PostConstruct
    public void postConstruct(){
        System.out.println("  BenzCar postConstruct....");
        if(engine == null){
            System.out.println(" postConstruct => engine not setting ~~~~");
        }else{
            System.out.println(" postConstruct  => engine 已经初始化");
        }
    }

}
