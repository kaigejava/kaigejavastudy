package com.kaigejava.spring.beanlife.config;

import com.kaigejava.spring.beanlife.bean.BenzCar;
import com.kaigejava.spring.beanlife.bean.specialbean.SpecialBeanForEngine;
import com.kaigejava.spring.beanlife.service.Engine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 15:01
 */
@Configuration
public class AppConfig {

    @Bean
    SpecialBeanForEngine specialBeanForEngine(){
        return new SpecialBeanForEngine();
    }


    @Bean(initMethod="startCar")
    BenzCar benzCar(Engine engine){
        BenzCar car = new BenzCar();
        car.engine = engine;
        return car ;
    }
}
