package com.kaigejava.spring.beanlife.service;

public interface Engine {
    void fire();
}
