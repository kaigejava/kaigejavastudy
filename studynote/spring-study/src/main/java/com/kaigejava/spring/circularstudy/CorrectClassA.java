package com.kaigejava.spring.circularstudy;

/**
 * @author 凯哥Java
 * @description  循环依赖-争取演示
 * @company
 * @since 2022/11/10 12:23
 */
public class CorrectClassA {

    private CorrectClassB correctClassB ;

    public void setCorrectClassA(CorrectClassB correctClassB) {
        this.correctClassB = correctClassB;
    }

    public CorrectClassB getCorrectClassA() {
        return correctClassB;
    }

    public void getB(){
        System.out.println(correctClassB.sayB());
    }


    public String sayA() {
        return  "CorrectClassA 中的sayA 方法";
    }
}
