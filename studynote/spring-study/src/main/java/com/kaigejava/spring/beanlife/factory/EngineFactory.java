package com.kaigejava.spring.beanlife.factory;

import com.kaigejava.spring.beanlife.service.Engine;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author 凯哥Java
 * @description  引擎工厂类
 * @company
 * @since 2022/11/10 15:17
 */
public class EngineFactory implements FactoryBean<Engine> , BeanNameAware, InvocationHandler {


    private String name;
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(" 引擎代理类的 invoke方法:"+method.getName());
        return null;
    }

    @Override
    public void setBeanName(String name) {
            this.name = name;
    }

    @Override
    public Engine getObject() throws Exception {
        System.out.println(" 引擎工厂类 开始创建引擎 。当前要创建的引擎name是:"+name);
        //创建代理对象
        Engine engineProxy = (Engine) Proxy.newProxyInstance(this.getClass().getClassLoader(),new Class[]{Engine.class},this);
        return engineProxy;
    }

    @Override
    public Class<?> getObjectType() {
        return Engine.class;
    }
}
