package com.kaigejava.spring.circularstudy;

/**
 * @author 凯哥Java
 * @description     循环依赖理解
 * @company
 * @since 2022/11/10 12:10
 */
public class CircularDemo {

    public static void main(String[] args) throws Exception {
        //error
       // errorTest();

        correctTest();


    }

    private static void correctTest() throws Exception {
        CorrectClassA classA = MyContainer.getBean(CorrectClassA.class);
        CorrectClassB classB = MyContainer.getBean(CorrectClassB.class);
        classA.getB();
        classB.getA();
    }

    private static void errorTest() {
        ErrorClassA classA = new ErrorClassA();
        ErrorClassB classB = new ErrorClassB();
    }

}
