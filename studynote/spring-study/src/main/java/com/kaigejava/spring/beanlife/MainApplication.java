package com.kaigejava.spring.beanlife;

import com.kaigejava.spring.beanlife.config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 14:49
 */
public class MainApplication {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfig.class);
        context.refresh();


    }
}
