package com.kaigejava.spring.circularstudy;

/**
 * @author 凯哥Java
 * @description 循环依赖-争取演示
 * @company
 * @since 2022/11/10 12:23
 */
public class CorrectClassB {

    private CorrectClassA correctClassA;

    public void setCorrectClassA(CorrectClassA correctClassA) {
        this.correctClassA = correctClassA;
    }

    public CorrectClassA getCorrectClassA() {
        return correctClassA;
    }

    public void getA(){
        System.out.println(correctClassA.sayA());
    }

    public String sayB() {
      return  "CorrectClassB 中的sayB 方法";
    }
}
