package com.kaigejava.jichu.jiekouchouxianglei.jiekou;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/1/28 21:00
 */
public class InterfaceImpl implements InterfaceA,InterfaceB{
    @Override
    public void say() {
        System.out.println("say");
    }

    @Override
    public void run() {
        System.out.println("run");

    }

    @Override
    public void eat() {
        System.out.println("eat");
    }


}
