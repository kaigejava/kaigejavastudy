package com.kaigejava.jichu.neibulei.darkcopy;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/11 20:37
 */
public class Address implements Cloneable{

    private String name;

    public Address(){}

    public Address(String name){
        this.name = name;
    }


    /**
     * 重写clone方法
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    protected Address clone() throws CloneNotSupportedException {
        return (Address)super.clone();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
