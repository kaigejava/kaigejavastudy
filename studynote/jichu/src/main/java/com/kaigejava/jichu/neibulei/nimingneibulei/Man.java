package com.kaigejava.jichu.neibulei.nimingneibulei;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 21:46
 */
public class Man implements  IPerson{

    private IPerson person;

    public Man(){}

    public Man(IPerson person){ this.person = person;}

    @Override
    public void say() {
        System.out.println("in man say ....");
    }

    public void start(){
        say();
        System.out.println("====");
        person.say();
    }

}
