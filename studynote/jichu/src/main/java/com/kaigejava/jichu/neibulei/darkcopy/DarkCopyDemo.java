package com.kaigejava.jichu.neibulei.darkcopy;

import java.util.HashSet;

/**
 * @author 凯哥Java
 * @description  深浅copy 理解demo
 * 浅拷贝：会在对上创建一个新的对象。但是如果原对象的内部属性是引用类型的话，浅拷贝会直接复用内部对象的地址
 * 深拷贝：深拷贝完全复制整个对象，包含这个对象所包含的内部对象
 * @company
 * @since 2022/11/11 20:30
 */
public class DarkCopyDemo {

    public static void main(String[] args) {
        int x = 128;
        int y = 128;
        Person p = new Person(new Address("北京"));
        Person p2 = p.clone();
        System.out.println("两个基本类型==后值:");
        System.out.println(x==y);
        System.out.println("两个对象(引用类型)==后值:");
        System.out.println(p == p2);
        System.out.println(" \n p的地址值为:"+p +" \n p2的地址值为:"+p2.toString());

        System.out.println(p.equals(p2));

        System.out.println("============");
        Girl gril1 = new  Girl();
        gril1.setAppearance("有一双亮晶晶的眼睛,一头乌黑的长发,一双灵巧的小手,一张红润润的小嘴,可爱极了");
        gril1.setSkinColour("玉骨冰肌");
        Girl gril2 = new  Girl();
        gril2.setAppearance("有一双亮晶晶的眼睛,一头乌黑的长发,一双灵巧的小手,一张红润润的小嘴,可爱极了");
        gril2.setSkinColour("玉骨冰肌");
        System.out.println(gril1.equals(gril2));
        System.out.println(" \n gril1的地址值为:"+gril1 +" \n gril2的地址值为:"+gril2);
        System.out.println(" \n gril1的hashCode为:"+gril1.hashCode() +" \n gril2的hashCode为:"+gril2.hashCode());

        HashSet set = new HashSet();
        set.add(gril1);
        set.add(gril2);
        System.out.println("set集合的大小为："+set.size());

      /*  DarkCopyDemo demo = new DarkCopyDemo();
        demo.shallowCopy();
        demo.darkCopy();*/
    }




    /**
     * 浅拷贝
     */
    private  void shallowCopy() {
        System.out.println("=====以下是浅 copy ======");
        Person p = new Person(new Address("北京"));
        Person p2 = p.clone();
        System.out.println("p.address:"+p.getAddress() +" \n p2.address:"+p2.getAddress() +" \n ");
        System.out.println("p:"+ p +" \n p2:"+p2);
    }

    private void darkCopy() {
        System.out.println("=====以下是 深 ======");
        Person p = new Person(new Address("北京"));
        Person p2 = p.darkClone();
        System.out.println("p.address:"+p.getAddress() +" \n p2.address:"+p2.getAddress() +" \n ");
        System.out.println("p:"+ p +" \n p2:"+p2);

    }
}
