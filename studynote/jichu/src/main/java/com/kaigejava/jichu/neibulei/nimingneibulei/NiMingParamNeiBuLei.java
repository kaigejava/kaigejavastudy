package com.kaigejava.jichu.neibulei.nimingneibulei;

/**
 * @author 凯哥Java
 * @description  匿名内部类 可以作为方法的参数形式传递
 * @company
 * @since 2022/11/9 21:26
 */
public class NiMingParamNeiBuLei {

    private static void paramCall(IPerson person){
        System.out.println("调用作为参数");
            person.say();
    }


    public static void main(String[] args) {
        paramCall(new IPerson() {
            @Override
            public void say() {
                System.out.println(" this param");
            }
        });
    }


}
