package com.kaigejava.jichu.neibulei.chengyuanneibulei;

/**
 * @author 凯哥Java
 * @description  内部类分为四种：成员内部类、静态内部类、匿名内部类、局部内部类四种
 * 本代码是成员内部类。
 * 成员内部类的特点：内部类可以访问外部类的所有方法和属性。但是外部类要访问内部类类的属性和方法，需要先实例化内部类才可以。
 * 需要注意：成员内部类中，不能包含静态属性和方法
 * @company
 * @since 2022/11/9 16:54
 */
public class Body {

     public String name=" my name is body";
    public void say(){
        System.out.println("body->say");
    }

    public void myHeart(){
        Heart heart = new Heart();
        heart.beat();
        heart.heartSay();
    }



    public void manHeart(){
        ManHeart mh = new ManHeart();
        mh.manHeart();;
    }

    /**
     * 成员内部类。其和成员变量一样
     */
    public class Heart{    //内部类
        public String name=" 内部类的Name: my Name is Heart";

        public void beat(){
            System.out.println("心脏跳动砰砰砰");
        }
        public void heartSay(){
            System.out.println("heart 对 body say:");
            say();
        }

        /**
         * 当内部类的属性名称和外部类的属性名称相同的时候,
         * 在内部类访问外部类的同名属性：外部类.this.属性名
         */
        public void getName(){
            System.out.println("heart 的name:"+this.name);
            System.out.println("获取外部类属性的方法是：外部类.this.属性:"+Body.this.name);
        }
    }

    public class ManHeart implements Iman{
        @Override
        public void manHeart() {
            System.out.println("成员内部类，可以访问同包下的类。这里是男人的心");
        }
    }
}
