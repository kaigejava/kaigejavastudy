package com.kaigejava.jichu.neibulei.nimingneibulei;

/**
 * @author 凯哥Java
 * @description 匿名内部类：除了没有类名之外，其他都一样
 * 1:正常的匿名内部类
 * 2：作为匿名内部类可以用作方法传递参数
 * @company
 * @since 2022/11/9 20:01
 */
public class NiMingNeiBuLei {

    public void callInner(){
        new IPerson(){
            @Override
            public void say() {
                System.out.println("这是匿名内部类的say方法");
            }
        }.say();
    }


    public static void main(String[] args) {
        NiMingNeiBuLei neiBuLei = new NiMingNeiBuLei();
        neiBuLei.callInner();;
    }


}
