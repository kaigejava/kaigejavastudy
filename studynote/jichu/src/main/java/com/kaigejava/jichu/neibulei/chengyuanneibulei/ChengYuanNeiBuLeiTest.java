package com.kaigejava.jichu.neibulei.chengyuanneibulei;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 17:26
 */
public class ChengYuanNeiBuLeiTest {

    public static void main(String[] args) {
        Body body  = new Body();
        body.myHeart();
        //成员内部类，访问同包下的
        body.manHeart();
        //当局部内部类的属性和外部类属性名字相同时候
        System.out.println("===========");
        Body.Heart heart = body.new Heart();
        heart.getName();

        //第二种
        System.out.println("====第二种=======");
        Body.Heart h2 = new Body().new Heart();


    }
}
