package com.kaigejava.jichu.neibulei.jubuneibulei;

/**
 * @author 凯哥Java
 * @description 内部类四种类型：局部内部类
 * 特点：
 * 1：类写在方法或者局部代码块中
 * 2：不能有权限修饰符及static修饰符。其修饰符同局部变量相同
 * 3：可以直接访问方法中的属性
 * 4：可以直接放完方法所在类中的属性和方法
 * 5：局部内部类声明：同局部变量声明类似。要在方法内部，局部内部类外部来声明
 * @company
 * @since 2022/11/9 18:52
 */
public class JuBuNeiBuLei {
    private String name = "方法所在类的属性";
    public int x = 10;

    public void show() {
        System.out.println("外部类的show方法被调用了");
    }

    public void juBu() {
        String name = "方法中的局部变量-Name";
        class Inner {
            String name = "局部内部类的属性-name";

            public void showInner(String name) {
                show();
                System.out.println("当前name为:" + name);
                System.out.println("====");
                System.out.println("获取外部类同名属性:" + JuBuNeiBuLei.this.name);
            }
        }
        //局部内部类声明：要在方法/局部代码块内部，内部类之外声明
        Inner inner = new Inner();
        inner.showInner(name);
    }


    public static void main(String[] args) {
        JuBuNeiBuLei neiBuLei = new JuBuNeiBuLei();
        neiBuLei.juBu();
    }

}
