package com.kaigejava.jichu.neibulei.darkcopy;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/11 20:40
 */
public class Person implements Cloneable{

    private Address address;

    public Person(){}

    public Person(Address address){
        this.address = address;
    }

    /**
     * 重写 obj的clone方法 。是浅copy
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    protected Person clone()  {
        try {
            return (Person)super.clone();
        }catch (Exception e){
            throw  new RuntimeException();
        }

    }

    /**
     * 深copy
     * @return
     */
    public Person darkClone(){
        try {
            Person p = clone();
            p.setAddress(p.getAddress().clone());
            return p;
        }catch (Exception e){
            throw  new RuntimeException();
        }
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }
}
