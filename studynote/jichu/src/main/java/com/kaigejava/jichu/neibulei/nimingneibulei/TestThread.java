package com.kaigejava.jichu.neibulei.nimingneibulei;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 22:07
 */
public class TestThread {

    public static void main(String[] args) throws InterruptedException {
        new Thread(){
            @Override
            public void run() {
                System.out.println(" new Thread ....");
            }
        }.start();

       Runnable r = new Runnable() {
           @Override
           public void run() {
               System.out.println(" runnable 1...1111 ");
           }
       };
       new Thread(r).start();


       new Thread(new Runnable() {
           @Override
           public void run() {
               System.out.println(" runnable 2...22222 ");
           }
       }).start();


       new Thread(()->{
           System.out.println("122");
       }).start();

       Thread.sleep(3000);
    }
}
