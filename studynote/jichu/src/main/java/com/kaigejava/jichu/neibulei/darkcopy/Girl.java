package com.kaigejava.jichu.neibulei.darkcopy;

import java.util.Objects;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/11 22:19
 */
public class Girl {

    private String appearance;

    private String skinColour;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Girl girl = (Girl) o;
        return Objects.equals(appearance, girl.appearance) &&
                Objects.equals(skinColour, girl.skinColour);
    }



    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public void setSkinColour(String skinColour) {
        this.skinColour = skinColour;
    }

    public String getAppearance() {
        return appearance;
    }

    public String getSkinColour() {
        return skinColour;
    }
}
