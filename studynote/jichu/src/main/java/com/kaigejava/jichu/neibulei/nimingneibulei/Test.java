package com.kaigejava.jichu.neibulei.nimingneibulei;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 21:55
 */
public class Test {

    public static void main(String[] args) {
        new Man() {
            @Override
            public void say() {
                System.out.println("Test.main ... ");
            }
        }.say();

        new IPerson() {

            @Override
            public void say() {
                System.out.println(" impl person");
            }
        }.say();



        new Man(new IPerson() {
            @Override
            public void say() {
                System.out.println(" param ....");
            }
        }).start();


        //上面简化
        new Man(()->{
            System.out.println("简化后的say");
        }).start();

        String [] arr = new String[]{};

    }
}
