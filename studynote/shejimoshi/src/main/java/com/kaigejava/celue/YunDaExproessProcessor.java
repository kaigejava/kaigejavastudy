package com.kaigejava.celue;

/**
 * @author 凯哥Java
 * @description  韵达快递
 * @company
 * @since 2022/11/10 19:33
 */
public class YunDaExproessProcessor  implements  ExpressProcessor{
    @Override
    public Integer getWeight() {
        return 30;
    }

    @Override
    public void process(String name,int weight) {
        System.out.println("= "+name+"快递 == 重量为:"+weight);
    }
}
