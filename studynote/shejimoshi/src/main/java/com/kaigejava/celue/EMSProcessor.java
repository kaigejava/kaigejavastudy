package com.kaigejava.celue;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 19:32
 */
public class EMSProcessor implements ExpressProcessor{
    @Override
    public Integer getWeight() {
        return 100;
    }

    @Override
    public void process(String name,int weight) {
        System.out.println("= "+name+"快递 == 重量为:"+weight);
    }
}
