package com.kaigejava.celue;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 19:31
 */
public class ShunfengExpressProcessor  implements  ExpressProcessor{
    @Override
    public Integer getWeight() {
        return 50;
    }

    @Override
    public void process(String name,int weight) {
        System.out.println("= "+name+"快递 == 重量为:"+weight);
    }
}
