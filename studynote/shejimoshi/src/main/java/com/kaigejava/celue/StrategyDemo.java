package com.kaigejava.celue;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 凯哥Java
 * @description 策略模式：定义了一组同类型的算法。在不同的类中封装起来，每种算法可以根据当前场景相互替换。从而使算法的变化独立于使用它们的客户端(既算法的调用者)
 * 使用场景：
 * 策略模式是解决过多if else 或者 swithc-case代码快方法之一。
 * 提高代码的可维护性、可扩展性和可读性
 * <p>
 * 策略模式三个角色：
 * 1：策略类：定义所有支持的算法的公共接口
 * 2：策略实现类：封装了具体的算法或行为，继承与策略类
 * 3：context上下文：维护一个starategy对象的应用。最终给客户端调用
 * @company
 * @since 2022/11/10 18:57
 */
public class StrategyDemo {

    public static void main(String[] args) {
        List<ExpressProcessor> expressProcessorList = new ArrayList<>(3);
        expressProcessorList.add(new EMSProcessor());
        expressProcessorList.add(new ShunfengExpressProcessor());
        expressProcessorList.add(new YunDaExproessProcessor());

        ExpressProcessor processor = null;
        DTO dto = new DTO("ShunFeng", 50);
        for (ExpressProcessor expressProcessor : expressProcessorList) {
            if (expressProcessor.getWeight() == dto.getWeight()) {
                processor = expressProcessor;
            }
        }

        processor.process(dto.getName(), dto.getWeight());
    }
}
