package com.kaigejava.celue;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 19:58
 */
public class DTO {

    private String name;

    private Integer weight;

    public DTO(){}

    public DTO(String name,Integer weight){
        this.name = name;
        this.weight = weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getWeight() {
        return weight;
    }
}
