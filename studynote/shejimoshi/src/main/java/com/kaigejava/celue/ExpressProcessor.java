package com.kaigejava.celue;

/**
 * 快递策略模式接口类
 */
public interface ExpressProcessor {

    /**
     * 获取重量
     * @return
     */
    Integer getWeight ();

    /**
     * 处理方法
     */
    void process(String name,int weight);
}
