package com.kaigejava.moban;

/**
 * @author 凯哥Java
 * @description 模板模式
 * 定义了一个算法步骤。允许子类为一个或多个不走提供其事件方法。让子类在不改变算法架构的清清裤下，重新定义算法中某些步骤
 * 优点：
 * 1：将系统处理逻辑代码放到抽象类中，可以提高代码的复用
 * 2：将不同代码放到不同子类中。通过对子类扩展增强新的行为。提高代码扩展性
 * 缺点：
 * 1：类增多
 * 2：如果父类有新方法，所有子类都要修改
 * <p>
 * 使用场景：
 * 1：有多个子类共用的方法，且逻辑相同的
 * 2：重要的，重复的方法，可以考虑作为模板方法
 * <p>
 * 模板方法包含主要角色：
 * 1：抽象模板类：一个抽象的类。包含了模板方法、具体方法、抽象方法、钩子方法
 * 2：方法分类：
 * a:模板方法：按照某种顺序调用其包含的方法(具体方法、抽象方法、钩子方法)
 * b：具体方法：在抽象类中已经事先的。在具体子类中可以继承或重写
 * c：抽象方法：在抽象类中声明，由子类来实现
 * d：钩子方法：在抽象类中已经实现了。包括用于判断的逻辑方法和需要子类重写的空方法
 * 3：具体模板子类：实现抽象类中所定义的抽象方法和钩子方法
 * @company
 * @since 2022/11/10 18:15
 */
public abstract class Template {
    //a：模板方法。将执行顺序的方法按照业务顺序处理好
    final public void templateMethod() {
        start();
        execute1();
        if (HookMethod2()) {
            execute2();
        }
        end();
    }

    /**
     * 具体方法
     */
    private void start() {
        System.out.println("启动审批流程。");
    }

    abstract void execute1();

    abstract void execute2();


    private void end() {
        System.out.println("审批流程结束。");
    }

    //钩子方法1
    void HookMethod1() {
        System.out.println("钩子方法1被执行");
    }

    // 钩子方法1
    boolean HookMethod2() {
        System.out.println("钩子方法2被执行");
        return true;
    }


}
