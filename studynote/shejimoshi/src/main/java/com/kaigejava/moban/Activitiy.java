package com.kaigejava.moban;

/**
 * @author 凯哥Java
 * @description activity工作流
 * @company
 * @since 2022/11/10 18:45
 */
public class Activitiy extends Template{
    @Override
    void execute1() {
        System.out.println("Activitiy exe1");
    }

    @Override
    void execute2() {
        System.out.println("Activitiy exe2");
    }


    @Override
    boolean HookMethod2() {
        return true;
    }
}
