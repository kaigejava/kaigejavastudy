package com.kaigejava.zerenlian.service.impl;

import com.kaigejava.zerenlian.DTO;
import com.kaigejava.zerenlian.service.BaseChkService;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 22:28
 */
public class ChkSuccess extends BaseChkService {
    @Override
    public boolean condition(DTO dto) {
        return true;
    }

    @Override
    protected void handler(DTO dto) {
        System.out.println("校验通过");
        dto.setFinish(false);
    }
}
