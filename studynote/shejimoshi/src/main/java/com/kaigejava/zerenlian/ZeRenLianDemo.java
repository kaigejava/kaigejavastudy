package com.kaigejava.zerenlian;

import com.kaigejava.zerenlian.service.IChkService;
import com.kaigejava.zerenlian.service.impl.ChkSuccess;
import com.kaigejava.zerenlian.service.impl.NameChk;
import com.kaigejava.zerenlian.service.impl.RoleChk;
import com.kaigejava.zerenlian.service.impl.PasswordChk;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 21:40
 */
public class ZeRenLianDemo {
    public static void main(String[] args) {
        List<IChkService> chkList  = new ArrayList<>();
        init(chkList);
    }

    private static void init(List<IChkService> chkList) {
        chkList.add( new NameChk());
        chkList.add(new PasswordChk());
        chkList.add(new RoleChk());
        chkList.add(new ChkSuccess());
        DTO dto = new DTO();
        dto.setName("admin");
        dto.setPassword("123456");
        List<String> roleList = new ArrayList<>();
        roleList.add("adminRole");
        dto.setRules(roleList);
        for (IChkService service : chkList) {
            service.run(dto);
        }
    }
}
