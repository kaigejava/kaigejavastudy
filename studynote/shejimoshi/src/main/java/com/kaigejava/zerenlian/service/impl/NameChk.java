package com.kaigejava.zerenlian.service.impl;

import com.kaigejava.zerenlian.service.BaseChkService;
import com.kaigejava.zerenlian.DTO;

/**
 * @author 凯哥Java
 * @description 校验名字的
 * @company
 * @since 2022/11/10 21:31
 */
public class NameChk extends BaseChkService {
    @Override
    public boolean condition(DTO dto) {
        if(!isEmpty(dto)){
            if("admin".equals(dto.getName())){
                return false;
            }
        }
        System.out.println("名字校验失败~");
        return true;
    }

    @Override
    protected void handler(DTO dto) {
        System.out.println("NameChk handler 名字校验失败~");
        dto.setFinish(true);
    }
}
