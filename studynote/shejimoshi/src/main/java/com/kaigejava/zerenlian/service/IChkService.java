package com.kaigejava.zerenlian.service;

import com.kaigejava.zerenlian.DTO;

public interface IChkService {

    void run(DTO dto);
}
