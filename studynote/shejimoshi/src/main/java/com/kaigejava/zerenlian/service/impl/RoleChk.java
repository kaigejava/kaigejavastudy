package com.kaigejava.zerenlian.service.impl;

import com.kaigejava.zerenlian.DTO;
import com.kaigejava.zerenlian.service.BaseChkService;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 21:39
 */
public class RoleChk extends BaseChkService {
    @Override
    public boolean condition(DTO dto) {
        if(!isEmpty(dto)){
            String adminRole = "adminRole";
            if(dto.getRules().contains(adminRole)){
                return false;
            }
        }
        System.out.println("角色校验是失败了~");

        return true;
    }

    @Override
    protected void handler(DTO dto) {
        dto.setFinish(true);
        System.out.println("RoleChk handler ok~");
    }
}
