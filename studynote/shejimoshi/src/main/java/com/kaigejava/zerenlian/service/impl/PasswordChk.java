package com.kaigejava.zerenlian.service.impl;

import com.kaigejava.zerenlian.DTO;
import com.kaigejava.zerenlian.service.BaseChkService;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 21:39
 */
public class PasswordChk extends BaseChkService {
    @Override
    public boolean condition(DTO dto) {
        if(!isEmpty(dto)){
            if("123456".equals(dto.getPassword())){
                return false;
            }
        }
        System.out.println("~~不满足密码校验条件~");
        return true;
    }

    @Override
    protected void handler(DTO dto) {
        dto.setFinish(false);
        System.out.println("PasswordChk handler ~~不满足密码校验条件~");
    }
}
