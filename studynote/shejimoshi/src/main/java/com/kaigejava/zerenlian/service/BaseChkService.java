package com.kaigejava.zerenlian.service;

import com.kaigejava.zerenlian.DTO;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 21:25
 */
public  abstract class BaseChkService implements IChkService{

    /**
     * 是否满足条件
     *
     * @return true-满足条件;false-不满足条件
     */
    protected abstract boolean condition(DTO DTO);

    @Override
    public void run(DTO dto) {
        if (baseCondition(dto)) {
            handler(dto);
            if (!dto.isFinish()) {
                finishNotice(dto);
            }
        }
    }

    public  void finishNotice(DTO dto){
        System.out.println("校验成功。当前用户："+dto.getName()+".欢迎登录~");
    }

    public  boolean baseCondition(DTO dto){

        if (dto.isFinish()) {
            return false;
        }
        return condition(dto);
    }
    public boolean isEmpty(DTO dto){
        if("".equals(dto.getName()) || null == dto.getName() ||
                "".equals(dto.getPassword()) || null == dto.getPassword() ||
                null == dto.getRules() || dto.getRules().size() == 1){
            return false;
        }
        return true;
    }


    protected abstract void handler(DTO DTO);
}
