package com.kaigejava.zerenlian;

import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 21:21
 */
public class DTO {

    private String name;

    private String password;

    private List<String> rules;

    /**
     * 是否完成
     */
    private boolean finish;

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public boolean isFinish() {
        return finish;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setRules(List<String> rules) {
        this.rules = rules;
    }

    public List<String> getRules() {
        return rules;
    }
}

