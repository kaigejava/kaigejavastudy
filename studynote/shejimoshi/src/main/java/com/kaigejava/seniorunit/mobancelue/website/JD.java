package com.kaigejava.seniorunit.mobancelue.website;

import com.kaigejava.seniorunit.mobancelue.pay.process.PayTypeList;
import com.kaigejava.seniorunit.mobancelue.ShoppingTemplate;
import com.kaigejava.seniorunit.mobancelue.pay.IPayProcess;

import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 20:30
 */
public class JD  extends ShoppingTemplate {

    public static int type = 1;
    List<IPayProcess> payList = PayTypeList.getPayList();
    @Override
    public void login(String userName, String password) {
        System.out.println("jd登录的用户名为:"+userName+"==密码为:"+password);
    }

    @Override
    public void pay(int payType, double money) {
        PayTypeList.doProcess(payType,money);
    }
}
