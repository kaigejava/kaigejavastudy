package com.kaigejava.seniorunit.mobancelue.pay;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 20:26
 */
public interface IPayProcess {

    int getPayType();


    String payProcess(double money);


}
