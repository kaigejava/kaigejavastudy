package com.kaigejava.seniorunit.mobancelue.pay.process;

import com.kaigejava.seniorunit.mobancelue.pay.IPayProcess;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 20:36
 */
public class PayTypeList {

    public static List<IPayProcess> getPayList(){
        List<IPayProcess> list = new ArrayList<>(2);
        list.add(new AliPayProcess());
        list.add(new JDPayProcess());
        return list;
    }

    public static void doProcess(int payType,double money){
        String payResult = "";
        for(IPayProcess payProcess :getPayList()){
            if(payType == payProcess.getPayType()){
                payResult =   payProcess.payProcess(money);
                break;
            }
        }
        System.out.println("支付结果为:"+payResult);
    }
}
