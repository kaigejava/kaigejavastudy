package com.kaigejava.seniorunit.mobancelue;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 20:10
 */
abstract public class ShoppingTemplate {

    private String url;

    public void openWebsite() {
        System.out.println("打开购物网站:" + url);
    }

    /**
     * 登录账号
     */
    protected abstract void login(String userName, String password);

    /**
     * 浏览商品
     */
    public void viewGoods() {
        System.out.println("开始浏览商品");
    }

    public void addCard() {
        System.out.println("开始加入购入车");
    }


    public abstract void pay(int payType, double money);


    public void end() {
        System.out.println("欢迎下次光临");
    }

    public void openUrl(String url) {
        this.url = url;
    }

}
