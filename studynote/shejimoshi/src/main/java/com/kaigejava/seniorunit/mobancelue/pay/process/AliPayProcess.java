package com.kaigejava.seniorunit.mobancelue.pay.process;

import com.kaigejava.seniorunit.mobancelue.pay.IPayProcess;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 20:26
 */
public class AliPayProcess implements IPayProcess {


    @Override
    public int getPayType() {
        return 0;
    }

    @Override
    public String payProcess(double money) {
        System.out.println("欢迎使用支付宝支付~.需要支付金额为:"+money);
        return "success";
    }
}
