package com.kaigejava.seniorunit.mobancelue.pay.process;

import com.kaigejava.seniorunit.mobancelue.pay.IPayProcess;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 20:28
 */
public class JDPayProcess implements IPayProcess {
    @Override
    public int getPayType() {
        return 1;
    }

    @Override
    public String payProcess(double money) {
        System.out.println("欢迎使用 京东  支付.需要支付金额为:"+money);
        return "error";
    }
}
