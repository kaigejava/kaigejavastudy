package com.kaigejava.seniorunit.mobancelue;

import com.kaigejava.seniorunit.mobancelue.pay.IPayProcess;
import com.kaigejava.seniorunit.mobancelue.pay.process.PayTypeList;
import com.kaigejava.seniorunit.mobancelue.website.JD;
import com.kaigejava.seniorunit.mobancelue.website.TB;

import java.util.List;

/**
 * @author 凯哥Java
 * @description  模板模和策略合用
 * @company
 * @since 2022/11/10 20:09
 */
public class MoBanAndCeLueDemo {

    public static void main(String[] args) {
        process(1);

    }

    private static void process(int type) {
        List<IPayProcess> payList = PayTypeList.getPayList();
        for(IPayProcess strategy : payList){
            if(strategy.getPayType() == type){
                if(type == JD.type){
                    JD jd = new JD();
                    jd.openUrl("www.jd.com");
                    jd.login("kaige","123");
                    jd.viewGoods();
                    jd.addCard();
                    jd.pay(type,20d);
                }else if(type == TB.type){
                    TB tb = new TB();
                    tb.openUrl("www.tb.com");
                    tb.login("jdkaige","456");
                    tb.viewGoods();
                    tb.addCard();
                    tb.pay(type,20d);
                }
            }
        }
    }
}
