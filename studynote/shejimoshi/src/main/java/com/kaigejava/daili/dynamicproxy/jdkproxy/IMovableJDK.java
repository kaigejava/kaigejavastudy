package com.kaigejava.daili.dynamicproxy.jdkproxy;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:21
 */
public interface IMovableJDK {

    String move(int x,int y);
}
