package com.kaigejava.daili.dynamicproxy.jdkproxy;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:30
 */
public class Car  implements IMovableJDK{
    @Override
    public String move(int x, int y) {
        System.out.println(" car 当前需要移动坐标为:"+x+","+y);
        return "car==》移动的目的地坐标为:"+x+","+y;
    }
}
