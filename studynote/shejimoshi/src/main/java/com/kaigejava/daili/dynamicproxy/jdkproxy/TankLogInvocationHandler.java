package com.kaigejava.daili.dynamicproxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:15
 */
public class TankLogInvocationHandler  implements InvocationHandler {

    private IMovableJDK movable;

    public TankLogInvocationHandler(IMovableJDK movable){
        this.movable = movable;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("== JDK动态代理处理类的 invoke方法。当前是执行前。获取方法名称为:"+method.getName());
        System.out.println("==args:"+args);
        Object  invokeResult = method.invoke(movable,args);
        System.out.println("== JDK动态代理处理类的 执行方法后。result:"+invokeResult);
        return invokeResult;
    }
}
