package com.kaigejava.daili.staticproxy.combination;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 16:55
 */
public class TankLogProxy implements IMovable{

    private IMovable movable;

    public TankLogProxy(IMovable movable){
        this.movable = movable;
    }
    @Override
    public void move() {
        System.out.println(" log日志代理对象。方法执行前操作。。。。。");
        movable.move();
        System.out.println("..... log日志代理对象。方法执行后操作。。。。。");
    }
}
