package com.kaigejava.daili.dynamicproxy.cglibproxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author 凯哥Java
 * @description  增强tank move方法的对象
 * @company
 * @since 2022/11/10 17:39
 */
public class EnhanceTankMethod implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("生成的类名" + o.getClass().getName());
        System.out.println("生成的类的父类" + o.getClass().getSuperclass().getName());
        System.out.println("方法执行前，被代理的方法" + method.getName());
        Object result = null;
        result = methodProxy.invokeSuper(o, objects);
        System.out.println("方法执行后，被代理的方法" + method.getName()+".result为："+result);
        return result;
    }
}
