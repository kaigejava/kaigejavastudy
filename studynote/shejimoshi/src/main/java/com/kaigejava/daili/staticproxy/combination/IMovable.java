package com.kaigejava.daili.staticproxy.combination;

public interface IMovable {
    void move();
}
