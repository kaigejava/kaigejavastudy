package com.kaigejava.daili.staticproxy.extendimpl;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 16:49
 */
public class ProxyTank extends Tank{

    @Override
    public void move() {
        System.out.println(" 这里是代理对象中的 move方法。在执行super.move之前。。。。");
        super.move();
        System.out.println(" 。。。。这里是代理对象中的 move方法。在执行super.move之后。。。。");

    }
}
