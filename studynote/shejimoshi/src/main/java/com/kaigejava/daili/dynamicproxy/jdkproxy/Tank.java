package com.kaigejava.daili.dynamicproxy.jdkproxy;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:06
 */
public class Tank implements IMovableJDK {


    @Override
    public String move(int x, int y) {
        System.out.println(" JDK 动态代理。接口实现类 Tank 中的。JDK动态代理，必须要实现一个接口.开始移动。当前需要移动坐标为:"+x+","+y);
        return "移动的目的地坐标为:"+x+","+y;
    }
}
