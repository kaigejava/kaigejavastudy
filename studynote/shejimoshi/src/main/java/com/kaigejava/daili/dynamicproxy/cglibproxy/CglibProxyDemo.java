package com.kaigejava.daili.dynamicproxy.cglibproxy;

import net.sf.cglib.proxy.Enhancer;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:46
 */
public class CglibProxyDemo {

    public static void main(String[] args) {
        //获取增强着对象
        Enhancer enhancer = new Enhancer();
        //设置父类
        enhancer.setSuperclass(TankCglib.class);
        //点被代理对下的方法被调用。该对象的intercept就会被调用
        enhancer.setCallback(new EnhanceTankMethod());
        TankCglib tankCglib = (TankCglib) enhancer.create();
        tankCglib.move(3,5);
    }
}
