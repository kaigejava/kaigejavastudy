package com.kaigejava.daili.dynamicproxy.cglibproxy;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:36
 */
public class TankCglib {

    public String move(int x,int y){
        System.out.println(" tankCglib对象中move");
        return "tcm x:"+x+", y:"+y;
    }
}
