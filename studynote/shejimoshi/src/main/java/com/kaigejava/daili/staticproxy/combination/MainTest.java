package com.kaigejava.daili.staticproxy.combination;

/**
 * @author 凯哥Java
 * @description
 * 静态代理分两种情况
 * 1：通过继承实现
 * 2：通过组合实现：多个类实现同一个接口。在其中代理类中,对方法做增强后，在代理方法中，实际还是调用目标的方法
 * 优点：可以代理所有实现接口的类
 * 缺点：被代理的类必须实现一个接口
 * JDK动态代理就是采用这种方式实现的。同样的代理类是有JDK自动帮我们在内存中生成的
 * @company
 * @since 2022/11/10 16:57
 */
public class MainTest {
    public static void main(String[] args) {
        new TankLogProxy(new TanK()).move();
        new TankLogProxy(new Car()).move();
    }
}
