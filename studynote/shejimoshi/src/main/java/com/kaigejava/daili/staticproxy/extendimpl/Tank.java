package com.kaigejava.daili.staticproxy.extendimpl;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 16:48
 */
public class Tank {

    public void move(){
        System.out.println("执行 Tank moving ~~~");
    }
}
