package com.kaigejava.daili.staticproxy.extendimpl;

/**
 * @author 凯哥Java
 * @description
 * 静态代理有俩种：
 * 1：通过继承方式实现
 *  优点：被代理类无需实现接口--其实就是正常的继承后重写了父类方法。
 *  缺点：只能代理这个类，要想代理其他类。需要写订的代理方法
 *  cglib动态代理就是采用了继承方式来进行代理的。只不过，具体的代理子类是由cglib在内存中生成
 * 2：通过组合实现
 * @company
 * @since 2022/11/10 16:47
 */
public class ExtendImplStaticProxy {
    public static void main(String[] args) {
        new ProxyTank().move();
    }
}
