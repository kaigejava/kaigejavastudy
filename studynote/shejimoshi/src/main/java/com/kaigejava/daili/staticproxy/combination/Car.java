package com.kaigejava.daili.staticproxy.combination;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 17:03
 */
public class Car implements IMovable {
    @Override
    public void move() {
        System.out.println("this in car move method· 。。。");
    }
}
