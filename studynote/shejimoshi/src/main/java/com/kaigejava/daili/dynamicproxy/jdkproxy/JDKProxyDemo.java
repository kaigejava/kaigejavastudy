package com.kaigejava.daili.dynamicproxy.jdkproxy;

import java.lang.reflect.Proxy;

/**
 * @author 凯哥Java
 * @description jdk动态代理
 * 优点：可以生成所有实现结款的代理对象
 * 缺点：必须面向接口
 * @company
 * @since 2022/11/10 17:05
 */
public class JDKProxyDemo {

    public static void main(String[] args) {
         Tank tank = new Tank();
        /**
         * newProxyInstance(ClassLoader loader,
         *                                           Class<?>[] interfaces,
         *                                           InvocationHandler h)
         */
        IMovableJDK o = (IMovableJDK) Proxy.newProxyInstance(JDKProxyDemo.class.getClassLoader(),new Class<?>[] {IMovableJDK.class},new TankLogInvocationHandler(tank));
        o.move(2,100);
        System.out.println("==========Tank 移动完成。轮到Car移动了==========");
        Car car = new Car();
        IMovableJDK o2 = (IMovableJDK) Proxy.newProxyInstance(JDKProxyDemo.class.getClassLoader()
                ,new Class<?> []{IMovableJDK.class}
                ,new TankLogInvocationHandler(car));
        o2.move(-300,-50);

    }

}
