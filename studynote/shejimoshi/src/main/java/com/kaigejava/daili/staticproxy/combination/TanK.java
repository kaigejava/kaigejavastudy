package com.kaigejava.daili.staticproxy.combination;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/10 16:54
 */
public class TanK implements IMovable{
    @Override
    public void move() {
        System.out.println("调用了 Tank 中的 move方法 。。。");
    }
}
