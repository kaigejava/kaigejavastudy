package com.kaigejava.thread.create;

/**
 * @author 凯哥Java
 * @description  第一种创建线程的方法：继承thread类。重写run方法.执行启动线程后，需要执行的业务类。
 * 一个线程启动是start方法。而不是run方法。run方法可以多吃执行。但是start方法只能一次
 * @company
 * @since 2022/11/9 20:41
 */
public class CreateExtendsThread extends   Thread{

    @Override
    public void run() {
        System.out.println("继承 thread类后，重写父类的run方法");
    }

    public static void main(String[] args) throws InterruptedException {
        CreateExtendsThread thread = new CreateExtendsThread();
        thread.start();
        Thread.sleep(3000);
        System.out.println("~~");
    }


    public void star2(){
        new Thread(){

        }.start();
    }
}
