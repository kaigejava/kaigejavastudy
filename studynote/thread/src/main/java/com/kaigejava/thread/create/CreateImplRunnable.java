package com.kaigejava.thread.create;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 21:05
 */
public class CreateImplRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println("实现runnable接口的run");
    }

    public static void main(String[] args) {
        CreateImplRunnable runnable = new CreateImplRunnable();
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
