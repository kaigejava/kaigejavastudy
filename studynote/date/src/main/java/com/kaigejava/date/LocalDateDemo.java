package com.kaigejava.date;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * locaDate类的demo
 * @author kaigejava
 */
public class LocalDateDemo {

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    private static String datePattern2 = "yyyy-MM-dd HH:mm:ss";

    /**
     * yyyy-MM-dd
     */
    private static String datePattern3 = "yyyy-MM-dd";

    SimpleDateFormat sdf = new SimpleDateFormat();

    @Test
    public void getToday(){
        LocalDate today = LocalDate.now();
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(datePattern3);
        String dateStr = today.format(fmt);
        System.out.println(dateStr);
    }


    /**
     * 获取
     * getYear:获取该日期的年份 int
     * getMonth：获取日期月份对象 i
     */
    public void getYMValue(){
        LocalDate today = LocalDate.now();
        System.out.println(today.getYear());
        System.out.println(today.getMonth());
        System.out.println(today.getMonthValue());
        System.out.println(today.getDayOfYear());
    }


}
