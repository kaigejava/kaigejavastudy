package com.kaigejava.springboonacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author kaigejava
 */
//@NacosPropertySource(dataId = "domain", autoRefreshed = true, type = ConfigType.YAML,groupId = "blog")
@SpringBootApplication
public class NacosApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosApplication.class, args);
    }

    /**
     * 监听Nacos加载
     *
     * @param config
     */
   // @NacosConfigListener(dataId = "domain", type = ConfigType.YAML,groupId = "blog")
    public void onMessage(String config) {
        System.out.println(config);
    }
}
