package com.kaigejava.springboonacos.controller;


import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author kaigejava
 */
@Controller
@RequestMapping("config")
public class NacosConfigController {

    @NacosValue(value = "${domain-url}", autoRefreshed = true)
    private String serverName;



    @RequestMapping(value = "/getString", method = GET)
    @ResponseBody
    public String getString(){
        return serverName;
    }
}
