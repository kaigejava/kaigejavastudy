package com.kaigejava.springboonacos.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author kaigejava
 */
@RestController
@RequestMapping("test")
public class TestController {


    @RequestMapping(value = "/{hi}", method = GET)
    @ResponseBody
    public String get(@PathVariable("hi") String hi) {
        return hi;
    }
}
