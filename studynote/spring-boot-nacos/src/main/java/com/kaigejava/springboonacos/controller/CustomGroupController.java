package com.kaigejava.springboonacos.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 自定义group配置-group从配置文件中获取的
 * @author kaigejava
 */
@Controller
@RequestMapping("group-config")
public class CustomGroupController {
    @NacosValue(value = "${domain-url}", autoRefreshed = true)
    private String serverName;

    @RequestMapping(value = "/getString", method = GET)
    @ResponseBody
    public String getString(){
        return serverName;
    }




    @NacosValue(value = "${domain-url}", autoRefreshed = true)
    private String payUrl;

    @RequestMapping(value = "/get-pay-url", method = GET)
    @ResponseBody
    public String getpayRul(){
        return serverName;
    }
}
