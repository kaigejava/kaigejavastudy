package com.kaigejava.commoneresult;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private int code;
    private String errorMsg;
    private Object data;
    private Long total;

    public static Result ok(){
        return new Result(200, null, null, null);
    }
    public static Result ok(Object data){
        return new Result(200, null, data, null);
    }
    public static Result ok(List<?> data, Long total){
        return new Result(200, null, data, total);
    }
    public static Result fail(String errorMsg){
        return new Result(400, errorMsg, null, null);
    }
}
