package com.kaigejava.codeing.wmmatch.wmmatch.d20221201;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/12/1 15:35
 */
public class StringExample {
    String str = new String("hello");
    char [] ch = {'a','b'};

    public void change(String str,char ch[]){
        str = "kaigejava ok";
        ch[0] = 'c';
    }
    public static void main(String[] args) {
        StringExample ex = new StringExample();
        ex.change(ex.str,ex.ch);
        System.out.println(ex.str + " and ");
        System.out.println(ex.ch);

    }
}
