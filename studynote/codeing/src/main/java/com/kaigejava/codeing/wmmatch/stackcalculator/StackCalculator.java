package com.kaigejava.codeing.wmmatch.stackcalculator;

/**
 * @author 凯哥Java
 * @description 使用栈模拟计算器
 * @company
 * @since 2022/11/18 10:14
 */
public class StackCalculator {

    public static void main(String[] args) {
        int coefficientArray[] = new int[]{2, 2};
        int exponentArray[] = new int[]{2, 4};
        CalculatorStack operaStack = new CalculatorStack(5);
        operaStack.push('+');
        operaStack.push('*');
        operaStack.push('*');

       // operaStack.show();
        String expressionResult = "";
        int result = 0;
        for(int i = 0;i<coefficientArray.length;i++){
            int opera = operaStack.pop();
            System.out.println("i="+i);
            String expression = operaStack.expression(coefficientArray[i],exponentArray[i],opera);
            int result1 = operaStack.cal(coefficientArray[i],exponentArray[i],opera);
            if(i %2== 0 ){
                expression += operaStack.expression(operaStack.pop());
                System.out.println("ii="+i);
            }
            expressionResult += expression;
            result+= result1;
        }

        System.out.println("表达式为:"+expressionResult +"。运行后的结果为:"+result);

    }
}
