package com.kaigejava.codeing.wmmatch.wmmatch.d20221123;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/23 19:22
 */
public class TwoArray {
    public static void main(String[] args) {
        int b[] [] = {{1},{2,3},{2,2,2}};
        int sum = 0;
        for(int i = 0;i<b.length;i++){
            for(int j = 0;j<b[i].length;j++){
                sum += b[i][j];
            }
        }
        System.out.println(sum);

        int [] a = {1,2,3,4,5,6};
        for(int i =0;i<6;i++){
            if(i%2 == 0){
                a[i]+=a[i+1];
            }
        }
    }
}
