package com.kaigejava.codeing.wmmatch.wmmatch.d20221201;

/**
 * @author 凯哥Java
 * @description 【面试题】Java中子类和父类静态代码块、非静态代码块、构造函数的执行顺序总结一览表的子类
 * @company
 * @since 2022/12/1 14:18
 */
public class A extends B{
    public A(){
        System.out.println("子类A的构造函数");
    }

    static {
        System.out.println("子类A的中的静态代码块");
    }

    {
        System.out.println("子类A的中的非静态代码块 sya()1");
    }

    public static void main(String[] args) {
        A a = new A();
        System.out.println("A!");
        A a2 = new A();
        System.out.println(a instanceof B);
        System.out.println(a instanceof A);

        System.out.println("启动完成");
    }
}
