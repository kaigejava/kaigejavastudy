package com.kaigejava.codeing.wmmatch.recursion;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 凯哥Java
 * @description 获取指定部门下的所有子部门-包含自己
 * @company
 * @since 2022/11/15 9:38
 */
public class AllDepartment {

    public static void main(String[] args) {
        AllDepartment allDepartment = new AllDepartment();
        List<Department> list = allDepartment.initDepartment();
        Department d1 = new Department();
        d1.setParentId(0);
        d1.setId(1);
        d1.setName("技术部");
        List<Department> myList = allDepartment.getMyAllDepartmentList(d1, list);
        myList.forEach(System.out::println);

    }

    private List<Department> getMyAllDepartmentList(Department d1, List<Department> list) {
        List<Department> result = new ArrayList<>();
        Integer myId = d1.getId();
        List<Department> mySonDepartment = list.stream().filter(d -> d.getParentId().equals(myId)).collect(Collectors.toList());
        result.add(d1);
        if (mySonDepartment.isEmpty()) {
            return result;
        }
        getSon(list, d1, result);
        return result;
    }

    private void getSon(List<Department> list, Department d1, List<Department> result) {
        Integer myId = d1.getId();
        List<Department> mySonDepartment = list.stream().filter(d -> d.getParentId().equals(myId)).collect(Collectors.toList());
        if (mySonDepartment.isEmpty() && !result.contains(d1)) {
            result.add(d1);
        } else {
            for (Department sonDdepartment : mySonDepartment) {
                result.add(sonDdepartment);
                getSon(list, sonDdepartment, result);
            }
        }

    }

    private List<Department> initDepartment() {
        List<Department> list = new ArrayList<>();
        Department d1 = new Department();
        d1.setParentId(0);
        d1.setId(1);
        d1.setName("技术部");

        Department d2 = new Department();
        d2.setParentId(0);
        d2.setId(2);
        d2.setName("人事部");


        Department d3 = new Department();
        d3.setParentId(0);
        d3.setId(3);
        d3.setName("财务部");

        Department d4 = new Department();
        d4.setParentId(1);
        d4.setId(4);
        d4.setName("大前端");

        Department d5 = new Department();
        d5.setParentId(1);
        d5.setId(5);
        d5.setName("后端");


        Department d6 = new Department();
        d6.setParentId(1);
        d6.setId(6);
        d6.setName("产品");


        Department d7 = new Department();
        d7.setParentId(5);
        d7.setId(7);
        d7.setName("Java");

        Department d8 = new Department();
        d8.setParentId(5);
        d8.setId(8);
        d8.setName("PHP");

        Department d9 = new Department();
        d9.setParentId(5);
        d9.setId(9);
        d9.setName("大数据");

        Department d10 = new Department();
        d10.setParentId(7);
        d10.setId(10);
        d10.setName("高级Java");

        Department d11 = new Department();
        d11.setParentId(9);
        d11.setId(11);
        d11.setName("大屏幕");

        Department d12 = new Department();
        d12.setParentId(4);
        d12.setId(12);
        d12.setName("IOS");

        Department d13 = new Department();
        d13.setParentId(2);
        d13.setId(13);
        d13.setName("HRBP");

        list.add(d1);
        list.add(d2);
        list.add(d3);
        list.add(d4);
        list.add(d5);
        list.add(d6);
        list.add(d7);
        list.add(d8);
        list.add(d9);
        list.add(d10);
        list.add(d11);
        list.add(d12);
        list.add(d13);
        return list;
    }
}


@Data
class Department {
    private Integer parentId;

    private Integer id;

    private String name;

}
