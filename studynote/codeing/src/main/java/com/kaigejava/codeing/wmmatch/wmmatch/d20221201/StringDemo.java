package com.kaigejava.codeing.wmmatch.wmmatch.d20221201;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/12/1 15:39
 */
public class StringDemo {

    private static final String MESSAGE = "kaigejava";

    public static void main(String[] args) {
        String a = "kaige"+"java";
        String b = "kaige";
        String c = "java";
        System.out.println(a==MESSAGE);
        System.out.println((b+c)==MESSAGE);

    }
}
