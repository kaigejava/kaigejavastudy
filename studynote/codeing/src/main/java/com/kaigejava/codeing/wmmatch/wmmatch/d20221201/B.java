package com.kaigejava.codeing.wmmatch.wmmatch.d20221201;

/**
 * @author 凯哥Java
 * @description  【面试题】Java中子类和父类静态代码块、非静态代码块、构造函数的执行顺序总结一览表的父类
 * @company
 * @since 2022/12/1 14:13
 */
public class B {

    private int x;

    public B(){
        System.out.println("父类B的构造函数");
    }

    static {
        System.out.println("父类B的中的静态代码块");
    }

    {
        System.out.println("父类B的中的非静态代码块 sya()");
    }
}
