package com.kaigejava.codeing.wmmatch.stackcalculator;

/**
 * @author 凯哥Java
 * @description 计算器的栈对象
 * @company
 * @since 2022/11/18 10:17
 */
public class CalculatorStack {

    private int maxSize;

    private int[] stack;

    private int top = -1;

    public CalculatorStack(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[maxSize];
    }

    /**
     * 获取栈顶的
     *
     * @return
     */
    public int peek() {
        return stack[top];
    }


    public void show() {
        if (isEmpty()) {
            System.out.println("当前栈为空");
            return;
        }

        for (int i = top; i >= 0; i--) {
            System.out.println("当前是栈中的第:" + i + "。当前的值为:" + stack[i]);
        }
    }

    public void push(int v) {
        if (isFull()) {
            System.out.println("当前栈已经满了");
            return;
        }
        //插入数据
        stack[++top] = v;
    }


    public Integer pop() {
        if (isEmpty()) {
            System.out.println("当前栈为空");
            return null;
        }
        Integer result = stack[top--];
        show();
        return result;
    }


    public int priority(int opera) {
        int priority = -1;
        char[] operas = {'*', '/', '+', '-'};
        if (opera == operas[0] || opera == operas[1]) {
            priority = 1;
        } else if (opera == operas[2] || opera == operas[3]) {
            priority = 0;
        }
        return priority;
    }


    public boolean isOpera(char v) {
        return ((v == '+' || v == '-') || (v == '*' || v == '-'));
    }

    public String expression(int num1, int num2, int opera) {
        String result = "";
        switch (opera) {
            case '*':
                result = num1 + "*" + num2;
                break;
            case '/':
                result = num2 + "/" + num1;
                break;
            case '+':
                result = num1 + "+" + num2;
                break;
            case '-':
                result = num2 + "-" + num1;
                break;
            default:
                break;
        }
        return result;
    }

    public String expression(int opera) {
        String result = "";
        switch (opera) {
            case '*':
                result = "*";
                break;
            case '/':
                result = "/";
                break;
            case '+':
                result = "+";
                break;
            case '-':
                result = "-";
                break;
            default:
                break;
        }
        return result;
    }


    public int cal(int num1, int num2, int opera) {
        int result = 0;
        switch (opera) {
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num2 / num1;
                break;
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num2 - num1;
                break;
            default:
                break;
        }
        return result;
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }

    public boolean isEmpty() {
        return (top == -1);
    }
}
