package com.kaigejava.codeing.wmmatch.baiduzhidao;

import java.util.*;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/12/5 3:56
 */
public class MiGong {
    public static void main(String[] args) {
        //设置迷宫大小 key：X轴 ；value:Y轴.这里也可以使用list存放
        Map<Integer, Integer> initSize = new HashMap<>(1);
        //起始位置是从0开始的。所以 7行21列就是 6,20
        initSize.put(6, 20);
        //设置每行中障碍情况 key:行数;value:每行障碍物的起止位置。
        // 比如第一行第7个位置为空。value表示为(5,7);一行可以有多个空位置的
        Map<Integer, Map<Integer, Integer>> barriersMap = new HashMap<>(7);
        Map<Integer, Integer> b1Map = new HashMap<>();
        b1Map.put(5, 7);
        b1Map.put(8, 10);
        //是从0开始的
        barriersMap.put(0, b1Map);


        Map<Integer, Integer> b2Map = new HashMap<>();
        b2Map.put(6, 6);
        b2Map.put(8, 10);

        //是从0开始的
       // barriersMap.put(1, b2Map);


        Set<Integer> initSizeKeySize = initSize.keySet();
        int x = 0, y = 0;
        for (int k : initSizeKeySize) {
            x = k;
            y = initSize.get(k);
        }
        //初始化迷宫
        List<String> strs = new ArrayList<>(x);
        for (int i = 0; i < x; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < y; j++) {
                //开始 障碍物
                Map<Integer, Integer> barrier = barriersMap.get(i);
                if (null == barrier || barrier.size() == 0) {
                   // System.out.print("0");
                    sb.append("0");
                    if (j == (y - 1)) {
                        // System.out.print("0 \r\n");
                        sb.append("0 \r\n");
                    }
                } else {
                    Set<Integer> barrierKey = barrier.keySet();
                    int start1 = -1;
                    int end1 = -1;
                    for (int start : barrierKey) {
                        start1 = start;
                        Integer end = barrier.get(start);
                        end1 = end;
                        //当是多个的时候,从上一个end开始
                        if (end >= end1) {
                            if (j != start && j != end) {
                                //System.out.print("0");
                                sb.append("0");
                                if (j == (y - 1)) {
                                   // System.out.print("0 \r\n");
                                    sb.append("0 \r\n");
                                }
                            } else {
                                System.out.print(" ");
                                sb.append("0 \r\n");
                                if (j == (y - 1)) {
                                    System.out.print("0 \r\n");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

