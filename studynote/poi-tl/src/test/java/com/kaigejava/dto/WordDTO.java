package com.kaigejava.dto;

import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.PictureType;
import com.deepoove.poi.data.Pictures;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.dto
 * @Author: kaigejava
 * @CreateTime: 2023-08-15  15:52
 * @Description: 导出word的对象
 * @Version: 1.0
 */
@Data
public class WordDTO {

    /**
     * 开始时间
     */
    private String start;

    /**
     * 结束时间
     */
    private String end;

    /**
     * 表格数据
     */
    private List<WordTableDTO> tableData;

    /**
     * 统计周期的天数
     */
    private String dayNum;

    /**
     * 周期内帽子数量
     */
    private String sumOnlineNum;

    /**
     * 在线率
     */
    private String sumOnlineRate;

    /**
     * 在线时长
     */
    private String sumOnlineTime;


    /**
     * 在线的
     */
    private String oneOnlineNum;


    /**
     * 在线率
     */
    private String oneOnlineRate;

    /**
     * 一的在线时间
     */
    private String oneOnlineTime;


    /**
     * 二的在线数量
     */
    private String twoOnlineNum;

    /**
     * 二的在线率
     */
    private String  twoOnlineRate;

    /**
     * 二的在线时间
     */
    private String twoOnlineTime;


    /**
     * 三的在线数量
     */
    private String threeOnlineNum;

    /**
     * 三的在线率
     */
    private String threeOnlineRate;

    /**
     * 三的在线总时长
     */
    private String threeOnlineTime;

    /**
     * 在线率最高的公司
     */
    private String maxRateCompany;


    /**
     * 最高的公司在线率
     */
    private String maxOnLineRate;

    /**
     * 最高的公司在线时长
     */
    private String maxOnLineTime;




    /**
     * 统计周期内在线总时长最多的
     */
    private String maxOnLineDay;

    /**
     * 本次统计内总在线时长
     */
    private String maxOnLineTimeLong;

    /**
     * 合格率
     */
    private String sumNoQualifiedNum;

    /**
     * 在线率最低的设备
     */
    private String minOnLineRateDevice;

    /**
     * 最低在线率
     */
    private String minOnLineRate;


    /**
     * 一部不合格的数量
     */
    private String oneNoQualifiedNum;

    /**
     * 一部不合格率
     */
    private String oneNoQualifiedRate;


    /**
     * 二部不合格的数量
     */
    private String twoNoQualifiedNum;


    /**
     * 二部不合格率
     */
    private String twoNoQualifiedRate;

    /**
     * 三部不合格数量
     */
    private String threeNoQualifiedNum;

    /**
     * 三部不合格率
     */
    private String threeNoQualifiedRate;

    /**
     * 最小在线率公司
     */
    private String minOnLineCompany;

    /**
     * 最小在线率部门
     */
    private String minOnLineDepartment;

    /**
     * 最小在线率名字
     */
    private String minOnLineRealName;


    /**
     * 最小在线日期
     */
    private String minOnLineDay;



    /**
     * 最小在线时长
     */
    private String minOnLineTime;



    /**
     * 大于3次的掉线情况
     */
    private String disconnectionDeviceNum;

    /**
     * 掉线名字
     */
    private String maxDisconnectionRealName;

    /**
     * 掉线日期
     */
    private String maxDisconnectionDate;

    /**
     * 掉线次数
     */
    private String maxDisconnectionNum;

    /**
     * 是否展示掉线的
     */
    private boolean showDisconnectionDeviceNum;

    /**
     * 释放展示正常
     */
    private boolean showEnd;

    /**
     * 是否展示最少在线公司
     */
    private boolean showMinOnLineCompany;

    /**
     * 图片1地址
     */
    private String  onePicturePosition;

    /**
     * 图片二
     */
    private String twoPicturePosition;

  


    public boolean getShowMinOnLineCompany() {
        return StringUtils.isNotBlank(minOnLineCompany);
    }

    public boolean getShowEnd() {
        return !getShowDisconnectionDeviceNum();
    }

    public boolean getShowDisconnectionDeviceNum() {
        if(StringUtils.isNoneBlank(disconnectionDeviceNum)){
            return Integer.valueOf(disconnectionDeviceNum)>0;
        }
        return StringUtils.isNotBlank(disconnectionDeviceNum);
    }
}
