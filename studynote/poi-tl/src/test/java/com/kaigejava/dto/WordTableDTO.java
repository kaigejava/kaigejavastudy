package com.kaigejava.dto;

import lombok.Data;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.dto
 * @Author: kaigejava
 * @CreateTime: 2023-08-15  16:16
 * @Description: word中表格对象
 * @Version: 1.0
 */
@Data
public class WordTableDTO {

    /**
     * 真实名称
     */
    private String  realName;
    /**
     * 公司名
     */
    private String company;

    /**
     * 部门
     */
    private String department;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 在线率
     */
    private String rate;

    /**
     * 在线总时长
     */
    private String time;
}
