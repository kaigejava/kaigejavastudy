package com.kaigejava.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author 凯哥Java
 * @description Redis启动类
 * @company
 * @since 2022/11/1 22:15
 */
@SpringBootApplication
@EnableCaching
public class RedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class,args);
        System.out.println("======================== Redis 项目启动完成! ========================");
    }
}
