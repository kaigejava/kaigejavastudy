package com.kaigejava.springbootkafka;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class web {
    @GetMapping("/hi/{message}")
    public String hi(@PathVariable("message") String msg){
        return "say:"+msg;
    }
}
