package com.kaigejava.springbootkafka.simpledemo.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class KafkaProducer {

    @Resource
    private KafkaTemplate<String,Object> kafkaTemplate;

    @GetMapping("/kafka/normal/{message}")
    public void simpledSendMessage(@PathVariable("message") String normalMessage){
        kafkaTemplate.send("test", normalMessage);
    }

}
