package com.kaigejava.springbootkafka;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaAppliction {
    public static void main(String[] args) {
        SpringApplication.run(KafkaAppliction.class, args);
    }
}
