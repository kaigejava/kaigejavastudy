package com.kaigejava.dto;

import lombok.Data;

import java.util.List;

/**
 * @author 凯哥Java
 * @description     添加参数对
 * @company         凯哥Java(http://www.kaigejava.com)
 * @since 2023/12/16 16:20
 */
@Data
public class DataRangeBaseParamDTO {

    /**
     * 站点集合
     */
    private List<String> webSite;
}
