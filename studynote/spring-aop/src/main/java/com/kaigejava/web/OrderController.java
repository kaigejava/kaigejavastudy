package com.kaigejava.web;

import com.kaigejava.aspect.annotates.ParamAspectAnnotate;
import com.kaigejava.service.OrderServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 凯哥Java
 * @description 订单类。模拟，使用@ParamAspectAnnotate注解的
 * @company 凯哥Java(http : / / www.kaigejava.com)
 * @since 2023/12/16 17:47
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private OrderServiceImpl orderService;


    @RequestMapping("get-username")
    @ParamAspectAnnotate
    public String getUserName() {
        return orderService.getUserName();
    }

    @RequestMapping("get-username2")
    // @ParamAspectAnnotate
    public String getUserName2() {
        return orderService.getUserName2();
    }

}
