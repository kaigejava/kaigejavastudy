package com.kaigejava.aspect.annotates;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * @author 凯哥Java
 * @description 自定义注解，标识需要添加datarange的
 * @company 凯哥Java(http : / / www.kaigejava.com)
 * @since 2023/12/16 17:34
 */


/**
 * @Author: kaigejava
 * @Date: 2023/12/13 11:42
 * @Description: 给参数设置数据范围的注解
 **/

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParamAspectAnnotate {
    String value() default "";
}
