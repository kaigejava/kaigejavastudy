package com.kaigejava.aspect.annotates;

import java.lang.annotation.*;

/**
 * @author 凯哥Java
 * @description    记录日志的自定义注解
 * @company         凯哥Java(http : / / www.kaigejava.com)
 * @since 2023/12/16 23:19
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogAspectAnnotate {
    String content() default "";//内容

    int sysType() default 0;//系统类型（管理平台，App端）

    int opType() default 0;//操作类型（0登录，1增加，2删除，3修改，4查询，5查看）
    String action() default "";//功能名称
}
