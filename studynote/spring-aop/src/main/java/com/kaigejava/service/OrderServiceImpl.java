package com.kaigejava.service;

import com.kaigejava.aspect.AddParamAspect;
import com.kaigejava.dto.DataRangeBaseParamDTO;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/12/16 19:45
 */
@Service
public class OrderServiceImpl {


    public String getUserName() {
        DataRangeBaseParamDTO dataRangeBaseParam = AddParamAspect.getDataRangeBaseParam();
        return dataRangeBaseParam.getWebSite().get(0);
    }

    public String getUserName2() {
        DataRangeBaseParamDTO dataRangeBaseParam = AddParamAspect.getDataRangeBaseParam();
        if(Objects.isNull(dataRangeBaseParam)){
            return "哦~凯哥。这里是空啊。请访问下凯哥Java的个人博客(http://www.kaigejava.com)吧";
        }
        return dataRangeBaseParam.getWebSite().get(1);
    }
}
