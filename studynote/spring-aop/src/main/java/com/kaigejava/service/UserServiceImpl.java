package com.kaigejava.service;

import com.kaigejava.aspect.AddParamAspect;
import com.kaigejava.dto.DataRangeBaseParamDTO;
import lombok.val;
import org.springframework.stereotype.Service;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/12/16 18:07
 */
@Service
public class UserServiceImpl {



    public String getUserName(){
        DataRangeBaseParamDTO dataRangeBaseParam = AddParamAspect.getDataRangeBaseParam();
        return dataRangeBaseParam.getWebSite().get(0);
    }

    public void getUserName2() {
        DataRangeBaseParamDTO dataRangeBaseParam = AddParamAspect.getDataRangeBaseParam();
        System.out.printf(dataRangeBaseParam.getWebSite().get(1));
    }
}
