package com.kaigejava.log;

import com.kaigejava.aspect.annotates.LogAspectAnnotate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 凯哥Java
 * @description         模拟添加了LogAspectAnnotate注解记录日志的
 * @company             凯哥Java(http : / / www.kaigejava.com)
 * @since 2023/12/17 21:03
 */
@RestController
@RequestMapping("admin")
@Slf4j
public class AdminController {


    @RequestMapping("find-all")
    @LogAspectAnnotate(opType  =4,action = "查询所有")
    public String findAll(String name){
        log.info("入参是:{}",name);
        return "kaigejava";
    }
}
