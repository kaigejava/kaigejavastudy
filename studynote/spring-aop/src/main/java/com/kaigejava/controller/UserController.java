package com.kaigejava.controller;

import com.kaigejava.service.UserServiceImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 凯哥Java
 * @description 用户controller.模拟此包下的所有方法都是切入点
 * @company 凯哥Java(http : / / www.kaigejava.com)
 * @since 2023/12/16 17:47
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private UserServiceImpl userService;


    @RequestMapping("get-username")
    public String getUserName() {
        return userService.getUserName();
    }


    @RequestMapping("get-username2")
    public void getUserName2() {
        userService.getUserName2();
    }


    @RequestMapping("get-usernam3")
    public void getUserName3(Integer id) {
        userService.getUserName2();
    }


    @RequestMapping("get-usernam4")
    public void getUserName4(Integer age) {
        if(age >25){
            throw  new RuntimeException("异常了");
        }
        userService.getUserName2();
    }

}
