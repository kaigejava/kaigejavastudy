package com.kaigejava.springdataredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/24 19:59
 */
@SpringBootApplication
public class SpringDataRedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringDataRedisApplication.class, args);
    }
}
