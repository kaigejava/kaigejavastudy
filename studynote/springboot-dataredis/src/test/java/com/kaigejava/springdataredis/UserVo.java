package com.kaigejava.springdataredis;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/24 20:54
 */
@Data
public class UserVo {

    private String name;
}
