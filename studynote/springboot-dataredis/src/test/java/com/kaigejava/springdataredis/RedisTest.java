package com.kaigejava.springdataredis;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/24 20:08
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testString(){
        redisTemplate.opsForValue().set("name","凯哥");
        Object v = redisTemplate.opsForValue().get("name");
        System.out.println(v);
    }


    @Test
    public void testString2(){
        UserVo userVo = new UserVo();
        userVo.setName("凯哥Java");
        redisTemplate.opsForValue().set("user:001",userVo);
        Object v = redisTemplate.opsForValue().get("user:001");
        System.out.println(v);
    }

    @Test
    public void testString3() throws IOException {
        UserVo userVo = new UserVo();
        userVo.setName("kaigeJava");
        ObjectMapper objectMapper = new ObjectMapper();
        String userStr = objectMapper.writeValueAsString(userVo);
        redisTemplate.opsForValue().set("user:001",userStr);

        String v = (String)redisTemplate.opsForValue().get("user:001");
        userVo = objectMapper.readValue(v,UserVo.class);
        System.out.println(userVo.getName());
    }

}
