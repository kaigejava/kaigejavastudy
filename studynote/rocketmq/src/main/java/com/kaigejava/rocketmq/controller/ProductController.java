package com.kaigejava.rocketmq.controller;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/10/18 13:46
 */
@RestController
public class ProductController {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping("/hi")
    public String hi(){
        return "say hi !";
    }

    @GetMapping("/rocketmq/product/{message}")
    public String simpledSendMessage(@PathVariable("message") String normalMessage){
        rocketMQTemplate.send("test-topic-1", MessageBuilder.withPayload("Hello, World! I'm from spring message"+normalMessage).build());
        return "发送的消息为:"+normalMessage;

    }
}
