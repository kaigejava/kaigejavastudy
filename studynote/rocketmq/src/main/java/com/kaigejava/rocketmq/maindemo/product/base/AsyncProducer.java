package com.kaigejava.rocketmq.maindemo.product.base;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.springframework.messaging.support.MessageBuilder;

/**
 * @author 凯哥Java
 * @description 异步
 * @company
 * @since 2022/10/18 19:04
 */
public class AsyncProducer {

    public static void main(String[] args) throws Exception {
        //1：创建消息生产者producer,并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("group1");
        //2：制定nameserver地址
        producer.setNamesrvAddr("192.168.50.131:9876");
        //设置发送超时时间：
        producer.setSendMsgTimeout(10000);

        //3：启动prodicer
        producer.start();
        //4：创建消息对象，指定主题Topic、Tag和消息体
        for (int i = 0; i < 1; i++) {
            Message message = new Message();
            message.setTopic("base-async-topic");
            message.setTags("Tag1");
            message.setBody(("from base-async-topic"+i).getBytes());
            //5：发送消息
            producer.send(message, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    System.out.println("异步发送消息,成功时候回调方法。%-10d ok %s %n."+sendResult.getMsgId());
                }

                @Override
                public void onException(Throwable throwable) {
                    System.out.println("异步发送消息,异常回调方法."+throwable.getMessage());

                }
            },60000);
        }
        //6：关闭生产者producer
        producer.shutdown();
    }
}
