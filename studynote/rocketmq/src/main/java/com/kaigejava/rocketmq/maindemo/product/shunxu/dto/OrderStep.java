package com.kaigejava.rocketmq.maindemo.product.shunxu.dto;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/10/19 9:48
 */
@Data
public class OrderStep {

    private Long orderId;
    private String desc;



}
