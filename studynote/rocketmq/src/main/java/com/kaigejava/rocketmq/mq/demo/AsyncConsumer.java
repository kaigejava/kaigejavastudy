package com.kaigejava.rocketmq.mq.demo;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author 凯哥Java
 * @description 异步消息的消费者
 * @company
 * @since 2022/10/18 18:31
 */
@Component
public class AsyncConsumer {
    @Slf4j
    @Service
    @RocketMQMessageListener(topic = "base-async-topic", consumerGroup = "my-consumer_test-topic-1")
    public static class MyConsumer1 implements RocketMQListener<String> {
        @Override
        public void onMessage(String message) {
            log.info("异步步消息,消费者收到消息为: {}", message);
        }
    }
}
