package com.kaigejava.rocketmq.controller.demo;

import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 凯哥Java
 * @description 发送单向消息
 * 这种方式主要用在不特别关心发送结果的场景。例如发送日志
 * @company
 * @since 2022/10/18 18:20
 */
@RestController
public class OneWayProducerController {
    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/rocketmq/oneway-product/{message}")
    public String simpledSendMessage(@PathVariable("message") String normalMessage){
        for (int i = 0; i < 10; i++) {
            rocketMQTemplate.sendOneWay("base-one-way-topic", MessageBuilder.withPayload("I'm from one-way-topic spring message"+i+normalMessage).build());
        }
        return "单向消息发送完成。发送的消息为:"+normalMessage;
    }
}
