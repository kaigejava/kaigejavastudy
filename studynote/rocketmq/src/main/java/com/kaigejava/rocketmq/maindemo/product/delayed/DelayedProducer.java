package com.kaigejava.rocketmq.maindemo.product.delayed;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;

/**
 * @author 凯哥Java
 * @description 延时消息的的生成者
 * @company
 * @since 2022/10/19 11:40
 */
public class DelayedProducer {
    public static void main(String[] args) throws Exception {
        //1：创建消息生产者producer,并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("group1");
        //2：制定nameserver地址
        producer.setNamesrvAddr("192.168.50.132:9876");
        //设置发送超时时间：
        producer.setSendMsgTimeout(10000);

        //3：启动prodicer
        producer.start();
        //4：创建消息对象，指定主题Topic、Tag和消息体
        for (int i = 0; i < 10; i++) {
            Message message = new Message();
            message.setTopic("delayed-topic");
            message.setTags("Tag1");
            message.setBody(("from delayed-main"+i).getBytes());
            message.setDelayTimeLevel(3);
            producer.send(message,60000);
        }
        System.out.println("延时发送完成");
        //6：关闭生产者producer
        producer.shutdown();
    }
}
