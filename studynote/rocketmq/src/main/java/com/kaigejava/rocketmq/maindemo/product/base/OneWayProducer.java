package com.kaigejava.rocketmq.maindemo.product.base;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;

/**
 * @author 凯哥Java
 * @description 单向
 * @company
 * @since 2022/10/19 8:43
 */
public class OneWayProducer {
    public static void main(String[] args) throws Exception {
        //1：创建消息生产者producer,并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("group1");
        //2：制定nameserver地址
        producer.setNamesrvAddr("192.168.50.132:9876");
        //设置发送超时时间：
        producer.setSendMsgTimeout(10000);

        //3：启动prodicer
        producer.start();
        //4：创建消息对象，指定主题Topic、Tag和消息体
        for (int i = 0; i < 10; i++) {
            Message message = new Message();
            message.setTopic("base-one-way-topic");
            message.setTags("Tag1");
            message.setBody(("from oneway-main"+i).getBytes());
            producer.sendOneway(message);
        }
        System.out.println("oneWay Ok");
        //6：关闭生产者producer
        producer.shutdown();
    }
}
