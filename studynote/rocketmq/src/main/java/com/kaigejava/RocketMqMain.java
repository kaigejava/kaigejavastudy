package com.kaigejava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/10/18 13:42
 */
@SpringBootApplication
public class RocketMqMain {
    public static void main(String[] args) {
        SpringApplication.run(RocketMqMain.class, args);
    }

}
