package com.kaigejava.dto;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description     MQTT数据的headers对象
 * @company HDTD
 * @since 2023/4/17 15:48
 */
@Data
public class HeadersInfoDTO {

    /**
     * 类型
     */
    private String messageType;

}
