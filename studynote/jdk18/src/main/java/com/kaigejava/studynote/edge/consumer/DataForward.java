package com.kaigejava.studynote.edge.consumer;

import com.kaigejava.studynote.edge.dto.ResultDTO;

import java.io.Serializable;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier.consumer
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  12:05
 * @Description: 数据转发的接口类
 * @Version: 1.0
 */
public interface DataForward  extends Consumer<ResultDTO> {
    void reportSystemInfo(Map<String, Serializable> properties);

}
