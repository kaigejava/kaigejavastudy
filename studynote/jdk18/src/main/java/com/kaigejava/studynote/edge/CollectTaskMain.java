package com.kaigejava.studynote.edge;

import com.kaigejava.studynote.edge.collect.impl.CollectTaskJobImpl;
import com.kaigejava.studynote.edge.consumer.impl.DataForwardImpl;
import com.kaigejava.studynote.edge.datahandler.impl.DataProcessingServiceImpl;
import com.kaigejava.studynote.edge.iotplatform.impl.IotPlatformClientImpl;
import com.kaigejava.studynote.supplier.SupplierTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  10:36
 * @Description: 模拟edge入口类
 * @Version: 1.0
 */
public class CollectTaskMain {

    public static void main(String[] args) throws InterruptedException {
        //启动成功后，开始消费队列中数据，进行上报
        IotPlatformClientImpl iotPlatformClient = BeanFactory.getBean();
        System.out.println("【项目启动完成】,延迟3秒后,进行数据上报...iotPlatformClient:"+iotPlatformClient);
        iotPlatformClient.initGateReportService(1);
        System.out.println("【项目启动完成】,其他任务.如网络设置、网关上线、脚本准备等等操作完成");

        System.out.println("1.获取到所有的定时任务；2.循环每个定时任务；3.根据任务id,先停止当前任务已经在运行的任务。4.开始信息任务【见下文】");
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(10);
        taskScheduler.setRemoveOnCancelPolicy(true);
        taskScheduler.setThreadNamePrefix("task-");
        CollectTaskMain test = new CollectTaskMain();
        //定时任务开始采集数据
        System.out.println("定时任务组装完成,启动");
        test.scheduleTest(taskScheduler);

        TimeUnit.SECONDS.sleep(500);
        System.out.println(1);
    }


    public void scheduleTest(ThreadPoolTaskScheduler taskScheduler) throws InterruptedException {
        //消费者
        DataForwardImpl comsumer = new DataForwardImpl();
        //数据二次处理
        DataProcessingServiceImpl dataProcessingService = new DataProcessingServiceImpl();
        // 设置任务调度器
        taskScheduler.initialize();
        // cron表达式
        CronTrigger cronTrigger = new CronTrigger("*/3 * * * * *");
        SupplierTest supplierTest = new SupplierTest();
        Supplier<List<String>> strListSupplier = () -> supplierTest.getListStr();
        Supplier<List<Integer>> intListSupplier = () -> supplierTest.getListInt();
        CollectTaskJobImpl jobDTO = new CollectTaskJobImpl(strListSupplier, intListSupplier, dataProcessingService, comsumer);
        // 提交任务
        ScheduledFuture<?> scheduledFuture = taskScheduler.schedule(jobDTO, cronTrigger);


    }
}
