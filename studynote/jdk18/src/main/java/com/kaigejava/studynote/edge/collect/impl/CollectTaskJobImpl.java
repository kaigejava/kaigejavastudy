package com.kaigejava.studynote.edge.collect.impl;

import com.kaigejava.studynote.edge.collect.CollectTaskJob;
import com.kaigejava.studynote.edge.datahandler.impl.DataProcessingServiceImpl;
import com.kaigejava.studynote.edge.dto.ResultDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier.dto
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  10:39
 * @Description: 采集实现类：采集+二次处理的
 * @Version: 1.0
 */
@Data
@Slf4j
@AllArgsConstructor
public class CollectTaskJobImpl implements CollectTaskJob {

    /**
     *字符串的
     */
    Supplier<List<String>> strListSupplier;

    /**
     * 数字的
     */
    Supplier<List<Integer>> intListSupplier;

    /**
     * 数据二次处理的
     */
    private DataProcessingServiceImpl dataProcessingService;

    private  Consumer<ResultDTO> consumer;




    @Override
    public ResultDTO exec() {
        List<String> names = strListSupplier.get();
        List<Integer> ids = intListSupplier.get();
        ResultDTO dto = new ResultDTO();
        dto.setTaskId(ids.get(0));
        dto.setTaskName(names.get(0));
       // System.out.println("组装后的dto:"+dto);
        if(Objects.nonNull(dataProcessingService)){
            //进行数据二次处理
            dto = dataProcessingService.handle(dto);
        }
        return dto;
    }
}
