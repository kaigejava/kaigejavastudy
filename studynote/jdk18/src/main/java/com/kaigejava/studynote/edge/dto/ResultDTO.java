package com.kaigejava.studynote.edge.dto;

import lombok.Data;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier.dto
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  10:48
 * @Description: 采集任务结果DTO对象
 * @Version: 1.0
 */
@Data
public class ResultDTO {

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务id
     */
    private Integer taskId;
}
