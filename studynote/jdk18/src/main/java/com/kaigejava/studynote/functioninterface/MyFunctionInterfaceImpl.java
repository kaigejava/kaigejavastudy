package com.kaigejava.studynote.functioninterface;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 20:24
 */
public class MyFunctionInterfaceImpl implements  IMyFunctionInterface{


    private IMyFunctionInterface rn;

    public MyFunctionInterfaceImpl(){}

    public  MyFunctionInterfaceImpl (IMyFunctionInterface rn){
        this.rn = rn;
    }

    @Override
    public void run() {
        System.out.println("123");
    }

    public void start(){
        rn.run();
    }
}
