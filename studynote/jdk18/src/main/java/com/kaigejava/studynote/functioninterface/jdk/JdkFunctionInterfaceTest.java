package com.kaigejava.studynote.functioninterface.jdk;

import java.util.function.*;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/2/26 18:12
 */
public class JdkFunctionInterfaceTest {

    public static void main(String[] args) {
        //没有输入，只要一个输出的使用supplier接口
        testSummlier();
        //只有一个输入，无输出的
        testConsumer();
        //输入是T，返回是R的
        testFunction();

        //输入和输出参数类型都一样的
        testUnaryOperator();
        //输入两个参数(T U )，返回一个(R)的
        testBigConsumer();


    }

    /**
     * 输入两个参数(T U )，返回一个(R)的
     */
    private static void testBigConsumer() {
        BiFunction<String,String,Integer> biFunction = (str1,str2)->Integer.valueOf(str1)+Integer.valueOf(str2);
        System.out.println("结果为"+biFunction.apply("1","3"));
    }

    /**
     * 输入和输出参数类型都一样的
     */
    private static void testUnaryOperator() {
        UnaryOperator<String> unaryOperator = str1->"这里是:"+str1;
        System.out.println( unaryOperator.apply("凯哥Java"));
    }


    /**
     * 输入是T，返回是R的
     */
    private static void testFunction() {
        Function<String,Integer> fu =  str1->Integer.valueOf(str1);
        System.out.println("使用function接口。输入是T,返回是R的结果:"+fu.apply("3"));
    }

    /**
     * 只有一个输入，无输出的
     */
    private static void testConsumer() {
        Consumer<String> consumer = message-> System.out.println("这里输出的内容是:"+message+".by kaigejava");
        consumer.accept("consumer 只有一个输入，无输出的");
    }


    /**
     * 没有输入，只要一个输出的使用supplier接口
     */
    private static void testSummlier() {
        Supplier<String> supplier = ()->"这是一个supplier接口的。只有一个输出的  by凯哥Java";
        System.out.println(supplier.get());
    }


}
