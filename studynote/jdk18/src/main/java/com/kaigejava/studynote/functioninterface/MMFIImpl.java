package com.kaigejava.studynote.functioninterface;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/9 20:28
 */
public class MMFIImpl implements  IMyFunctionInterface{
    @Override
    public void run() {
        System.out.println(" implements interface . run ");
    }

    public static void main(String[] args) {
        MMFIImpl mmfi = new MMFIImpl();
        MyFunctionInterfaceImpl myFunctionInterfaceImpl = new MyFunctionInterfaceImpl(mmfi);
        myFunctionInterfaceImpl.start();
    }
}
