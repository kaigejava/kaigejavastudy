package com.kaigejava.studynote.edge.datahandler;

import com.kaigejava.studynote.edge.dto.ResultDTO;

/**
 * @Author: kaigejava
 * @Date: 2023/12/6 12:53
 * @Description: 二次数据处理接口
 **/

public interface DataProcessingService {
    /**
     * 二次处理
     * @return 处理后的数据
     */
    ResultDTO handle(ResultDTO dto);
}
