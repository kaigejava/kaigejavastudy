package com.kaigejava.studynote.functioninterface;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/2/26 15:55
 */
public class FunctionTest {

    public void callMyNewFunction(MyNewFunction myNewFunction){
        System.out.println("调用了MyNewFunction的方法");
        myNewFunction.run();
    }
    public static void main(String[] args) {
        FunctionTest test = new FunctionTest();
        test.callMyNewFunction(()-> System.out.println("凯哥Java"));
    }
}
