package com.kaigejava.studynote.consumer;
import java.util.function.Consumer;

import org.junit.Test;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.consumer
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  13:34
 * @Description: consumer测试类
 * @Version: 1.0
 */
public class ConsumerTest {
    /**
     * consumer<T>:消费性接口
     * void accept(T t):对类型为T的对象应用操作
     * 特点：
     * 入参是T类型的，无返回值
     * 使用场景：
     * 如果需要一个有参返回值的函数式接口，那么我们就可以使用Consumer函数式即可，无需自定义函数式
     */

    /**
     * 请客喝水
     */
    @Test
    public void dink(){

        //这里模拟额外处理
        Consumer<Double> consumer = a ->{
            System.out.println("本次请喝水消耗:"+a+"个金豆");
        } ;
        shop(consumer,12.5);
    }

    private void shop(Consumer<Double> consumer, double money) {
        consumer.accept(money);
    }


}
