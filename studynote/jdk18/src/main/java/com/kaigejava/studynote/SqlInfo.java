package com.kaigejava.studynote;

import lombok.Data;

/**
 * @description
 * @since 2022/5/7 11:08
 */
@Data
public class SqlInfo {

    private Integer dbNo;

    private Integer tableNo;

    private String signSql;
    private String memberSignSql;

    private Long userId;


}
