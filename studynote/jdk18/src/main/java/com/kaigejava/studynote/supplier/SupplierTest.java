package com.kaigejava.studynote.supplier;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.function.Supplier;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  10:18
 * @Description: 测试jdk1.8的Supplier的
 * @Version: 1.0
 */
public class SupplierTest {

    /**
     * Supplier<T>：供给型接口
     * T get():返回类型为T的对象
     */
    @Test
    public void test1(){
        Supplier<List<String>> listSupplier = () ->getListStr();
        List<String> strings = listSupplier.get();
        strings.forEach(System.out::println);

    }

    public List<String> getListStr() {
        List<String> list = Lists.newArrayList();
        for (int i = 0; i < 1; i++) {
            list.add("凯哥Java");
        }
        return list;
    }

    public List<Integer> getListInt() {
        List<Integer> list = Lists.newArrayList();
        for (int i = 0; i < 1; i++) {
            list.add(i+1);
        }
        return list;
    }
}
