package com.kaigejava.studynote.edge;

import com.kaigejava.studynote.edge.iotplatform.impl.IotPlatformClientImpl;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.edge
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  17:43
 * @Description: 模拟spring生产单例bean对象
 * @Version: 1.0
 */
public class BeanFactory {

   private static final  IotPlatformClientImpl iotPlatformClient =  new IotPlatformClientImpl();
    private  BeanFactory(){}

    public static IotPlatformClientImpl getBean(){
        return iotPlatformClient;
    }

}
