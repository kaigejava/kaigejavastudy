package com.kaigejava.studynote.functioninterface;

/**
 * @author 凯哥Java
 * @description         接口函数式编程
 * @company
 * @since 2023/2/26 15:55
 */
@FunctionalInterface
public interface MyNewFunction {

    public abstract void run();
}
