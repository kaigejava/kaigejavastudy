package com.kaigejava.studynote.edge.datahandler.impl;

import com.kaigejava.studynote.edge.datahandler.DataProcessingService;
import com.kaigejava.studynote.edge.dto.ResultDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier.datahandler.impl
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  12:53
 * @Description: 二次数据处理实现类
 * @Version: 1.0
 */
public class DataProcessingServiceImpl implements DataProcessingService {


    @Override
    public ResultDTO handle(ResultDTO dto) {
        dto.setTaskName(dto.getTaskName()+":kaigejava");
        return dto;
    }
}
