package com.kaigejava.studynote;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * jdk1.8 集合相关的API
 * @author kaigejava
 */
@Slf4j
public class JDK18CollectionDemo {


    /**
     * 获取ids的
     * 语法：
     *  先map,在map里面使用对象获取对应的属性;
     *  然后在collect(Collectors.toList())
     */
    @Test
    public void getIdsDemo(){
        List<EmployeeInfo> employeeInfoList = EmployeeInfo.getList();
        List<Integer> ids = employeeInfoList.stream().map(EmployeeInfo::getId).collect(Collectors.toList());
        log.info("ids:{}",ids);
    }

    /**
     * 转map
     */
    @Test
    public void toMapTest(){
        List<EmployeeInfo> employeeInfoList = EmployeeInfo.getList();
        Map<Integer,EmployeeInfo> map = employeeInfoList.stream().collect(Collectors.toMap(EmployeeInfo::getId,a->a,(a,b)->b));
        log.info("map:{}",map);
    }

    /**
     * 过滤1
     * 获取年龄小于20岁的
     * 获取小于20岁的数量
     */
    @Test
    public void filterDemo1(){
        List<EmployeeInfo> employeeInfoList = EmployeeInfo.getList();
        List<EmployeeInfo> list = employeeInfoList.stream().filter(info->info.getAge()>20).collect(Collectors.toList());
        log.info("list:{}",list.toString());

        long count = employeeInfoList.stream().filter(info->info.getAge()>20).count();
        log.info("总数为:{}",count);
    }


    @Test
    public void filterDemo2(){
        List<EmployeeInfo> employeeInfoList = EmployeeInfo.getList();
        List<EmployeeInfo> list = employeeInfoList.stream().filter(info->info.getName().contains("acer")).collect(Collectors.toList());
        log.info("size:{}",list.size());
        log.info("list:{}",list.toString());
        List<EmployeeInfo> list2 = employeeInfoList.stream().filter(info->info.getName().equals("acer")).collect(Collectors.toList());
        log.info("size2:{}",list2.size());
        log.info("list2:{}",list2.toString());

    }

    /**
     * 根据部门分组，
     */
    @Test
    public void groupByDemo(){
        List<EmployeeInfo> employeeInfoList = EmployeeInfo.getList();
        Map<String,List<EmployeeInfo>> map = employeeInfoList.stream().collect(Collectors.groupingBy(EmployeeInfo::getDepartment));
        log.info("map:{}",map);
    }



}
