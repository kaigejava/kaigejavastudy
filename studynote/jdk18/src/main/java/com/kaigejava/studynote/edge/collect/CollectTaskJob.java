package com.kaigejava.studynote.edge.collect;

import com.kaigejava.studynote.edge.dto.ResultDTO;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier.dto
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  10:46
 * @Description: 采集任务接口
 * @Version: 1.0
 */
public interface CollectTaskJob extends  Runnable{

    Consumer<ResultDTO> getConsumer();

    /**
     * 执行
     * @return 组装后的任务对象
     */
    ResultDTO exec();

    @Override
   default void run() {
        ResultDTO task = this.exec();
        if(Objects.nonNull(task) && Objects.nonNull(getConsumer())){
            getConsumer().accept(task);
        }
    }
}
