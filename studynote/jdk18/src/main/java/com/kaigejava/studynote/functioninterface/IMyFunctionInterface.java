package com.kaigejava.studynote.functioninterface;

@FunctionalInterface
public interface IMyFunctionInterface {

    void run();
}
