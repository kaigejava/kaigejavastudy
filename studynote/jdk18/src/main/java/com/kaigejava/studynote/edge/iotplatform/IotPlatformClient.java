package com.kaigejava.studynote.edge.iotplatform;

import com.kaigejava.studynote.edge.dto.ResultDTO;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.edge.iotplatform
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  15:08
 * @Description: 物联网云平台端相关操作的
 * @Version: 1.0
 */
public interface IotPlatformClient {

    /**
     * 上报平台；这里需要从队列中获取数据，排队上报给云平台
     * @param resultDTO         采集到数据结果对象
     * @param time              当前时间
     */
    void reportToIot(ResultDTO resultDTO,Long time);
}
