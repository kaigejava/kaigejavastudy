package com.kaigejava.studynote.lamdba;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/2/26 15:19
 */
public class ThreadLambdaDemo {

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("这里是凯哥Java");
            }
        }).start();

        //使用lambda表达式的写法
        System.out.println("使用lambda表达式写法~");
        new Thread(()->System.out.println("这里是凯哥Java")).start();
    }
}
