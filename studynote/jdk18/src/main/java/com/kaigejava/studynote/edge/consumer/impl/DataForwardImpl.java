package com.kaigejava.studynote.edge.consumer.impl;

import com.kaigejava.studynote.edge.BeanFactory;
import com.kaigejava.studynote.edge.consumer.DataForward;
import com.kaigejava.studynote.edge.dto.ResultDTO;
import com.kaigejava.studynote.edge.iotplatform.impl.IotPlatformClientImpl;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @BelongsProject: kaigejavastudy
 * @BelongsPackage: com.kaigejava.studynote.supplier.consumer.impl
 * @Author: kaigejava
 * @CreateTime: 2023-12-06  12:46
 * @Description: 数据转发的实现类(也是consumer<T>的实现类)
 * @Version: 1.0
 */
@Slf4j
public class DataForwardImpl implements DataForward {
    @Override
    public void reportSystemInfo(Map<String, Serializable> properties) {
        System.out.println("需要上报的属性:"+properties);
    }

    @Override
    public void accept(ResultDTO resultDTO) {
      // log.info("消费者accept方法的数据:{}",resultDTO);
       //将数据转发到平台
        IotPlatformClientImpl iotPlatformClient = BeanFactory.getBean();
       // System.out.println("accept....iotPlatformClient:"+iotPlatformClient);
        iotPlatformClient.reportToIot(resultDTO,new Date().getTime());
    }
}
