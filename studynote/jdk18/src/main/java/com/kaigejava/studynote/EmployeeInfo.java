package com.kaigejava.studynote;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author kaigejava
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmployeeInfo {

    private Integer id;

    private String name;

    private Integer age;

    private String department;

    private String addr;





    public static List<EmployeeInfo> getList(){
        List<EmployeeInfo> list = Lists.newArrayList();
        list.add(new EmployeeInfo(1,"黑客",18,"技术部","北京"));
        list.add(new EmployeeInfo(2,"马云",28,"销售部","杭州"));
        list.add(new EmployeeInfo(3,"马化腾",24,"技术部","深圳"));
        list.add(new EmployeeInfo(4,"任正非",38,"CEO","深圳"));
        list.add(new EmployeeInfo(5,"李彦宏",23,"技术部","北京"));
        list.add(new EmployeeInfo(6,"周鸿祎",24,"技术部","北京"));
        list.add(new EmployeeInfo(7,"Acer",24,"技术部","北京"));
        list.add(new EmployeeInfo(8,"acer",24,"技术部","北京"));
        list.add(new EmployeeInfo(9,"Acer1",24,"技术部","北京"));
        list.add(new EmployeeInfo(10,"acer2",24,"技术部","北京"));
        list.add(new EmployeeInfo(11,"Acer1哈哈",24,"技术部","北京"));
        list.add(new EmployeeInfo(12,"acer2哈哈",24,"技术部","北京"));
        list.add(new EmployeeInfo(13,"Acer1cp",24,"技术部","北京"));
        list.add(new EmployeeInfo(14,"acer2Cp",24,"技术部","北京"));

        return list;
    }
}
