package com.kaigejava.jvmoom.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/3/28 23:26
 */
@RestController
public class JvmOOMController {

    private static final int _1MB = 1024 * 1024*10;

    /**
     * -Xms100m -Xmx100m -XX:+PrintGCDetails
     * @param msg
     * @return
     */
    @GetMapping("/test")
    public String test(Integer msg){
        System.out.println("msg:"+msg);
        if(msg == 3){
            tst();
           }
        return "OK"+msg;
    }

    public void tst(){
        List<ByteBuffer> list = new ArrayList<ByteBuffer>();
        System.out.println();
        int x = 0;
        while (x<=5000){
            System.out.println("XXX="+x);
            ByteBuffer buffer = ByteBuffer.allocateDirect(_1MB);
            list.add(buffer);
            x ++;
        }

    }

}
