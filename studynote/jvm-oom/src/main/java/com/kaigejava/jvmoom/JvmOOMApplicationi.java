package com.kaigejava.jvmoom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 凯哥Java
 * @description Redis启动类
 * @company
 * @since 2022/11/1 22:15
 */
@SpringBootApplication
public class JvmOOMApplicationi {
    public static void main(String[] args) {
        SpringApplication.run(JvmOOMApplicationi.class, args);
        System.out.println("========================  项目启动完成! ========================");
    }
}
