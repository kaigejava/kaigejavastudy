package com.kaigejava.dto;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/8/13 11:05
 */
@Data
public class SystemMessageDTO {

    private String  title;


    private String time;


    private int status;
}
