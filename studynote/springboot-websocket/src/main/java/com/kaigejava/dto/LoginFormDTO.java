package com.kaigejava.dto;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/8/13 9:46
 */
@Data
public class LoginFormDTO {
    private String phone;
    private String code;
    private String password;
}
