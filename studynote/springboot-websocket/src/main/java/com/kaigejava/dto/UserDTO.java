package com.kaigejava.dto;

import lombok.Data;

@Data
public class UserDTO {
    private String id;
    private String nickName;
    private String icon;
}
