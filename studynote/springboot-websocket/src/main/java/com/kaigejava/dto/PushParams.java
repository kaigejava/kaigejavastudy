package com.kaigejava.dto;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/8/13 10:41
 */

@Data
public class PushParams {

    /**
     * 功能描述:
     * 类型
     */
    private String type;

    /**
     * 功能描述:
     * 开始时间
     */
    private String startTime;

    /**
     * 功能描述:
     * 结束时间
     */
    private String stopTime;
}

