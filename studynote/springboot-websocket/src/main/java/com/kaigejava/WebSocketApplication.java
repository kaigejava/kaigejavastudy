package com.kaigejava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 凯哥Java
 * @description Redis启动类
 * @company
 * @since 2022/11/1 22:15
 */
@SpringBootApplication
public class WebSocketApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebSocketApplication.class, args);
        System.out.println("======================== websocket 项目启动完成! ========================");
    }
}
