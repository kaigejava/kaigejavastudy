package com.kaigejava.systemmessage;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.kaigejava.commoneresult.Result;
import com.kaigejava.service.WebSocketServer;
import com.kaigejava.util.RedisUtil;
import com.kaigejava.dto.SystemMessageDTO;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.kaigejava.service.RedisConstant.KEY;
import static com.kaigejava.service.RedisConstant.TOTAL_KEY;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/8/13 9:55
 */
@RestController
@RequestMapping("msg")
 @CrossOrigin(origins = "*",maxAge = 3600)
public class SystemMessageController {


    @Resource
    private RedisUtil redisUtil;

    @Resource
    private WebSocketServer webSocketServer;

    @GetMapping("/test")
    public String test(String msg) {
        redisUtil.set("kaige-pp", "价格:" + msg);
        return "OK";
    }


    @GetMapping("/unread_list/{userId}")
    public Result unreadList(@PathVariable("userId") String userId) {
        //这里将消息存放到redis中
        String redisKey = KEY + userId;
        Object megObj = redisUtil.get(redisKey);
        List<SystemMessageDTO> list = new ArrayList<SystemMessageDTO>();
        if (Objects.nonNull(megObj)) {
            list = JSONUtil.toList((String) megObj, SystemMessageDTO.class);
        }
        List<SystemMessageDTO> unreadList = list.stream().filter(dto -> dto.getStatus() == 0).collect(Collectors.toList());
        List<SystemMessageDTO> dataList = new ArrayList<SystemMessageDTO>();
        if (!CollectionUtils.isEmpty(unreadList)) {
            for (int i = 0; i < unreadList.size(); i++) {
                SystemMessageDTO newDto = new SystemMessageDTO();
                BeanUtil.copyProperties(list.get(i), newDto);
                dataList.add(newDto);
            }
            //将这些设置为已读。
            list.stream().forEach(dto -> dto.setStatus(1));
            //重新设置回去
            redisUtil.set(redisKey, JSONUtil.toJsonStr(list));
            //设置未读数量为0个
            String rediske = TOTAL_KEY + userId;
            redisUtil.set(rediske, 0);
            //推送
            webSocketServer.sendMessageTotalByUserId(userId);
        }

        return Result.ok(dataList);
    }

    @GetMapping("/list/{userId}")
    public Result list(@PathVariable("userId") String userId) {
        //redisUtil.set("kaige-pp", "价格:" + msg);
        //这里将消息存放到redis中
        String redisKey = KEY + userId;
        Object megObj = redisUtil.get(redisKey);
        List<SystemMessageDTO> list = new ArrayList<>();
        if (Objects.nonNull(megObj)) {
            list = JSONUtil.toList((String) megObj, SystemMessageDTO.class);
        }

        webSocketServer.sendMessageTotalByUserId(userId);
        List<SystemMessageDTO> unreadList = list.stream().filter(dto -> dto.getStatus() == 0).collect(Collectors.toList());
        List<SystemMessageDTO> dataList = new ArrayList<SystemMessageDTO>();
        if (!CollectionUtils.isEmpty(unreadList)) {
            for (int i = 0; i < unreadList.size(); i++) {
                SystemMessageDTO newDto = new SystemMessageDTO();
                BeanUtil.copyProperties(list.get(i), newDto);
                dataList.add(newDto);
            }
            //将这些设置为已读。
            list.stream().forEach(dto -> dto.setStatus(1));
            //重新设置回去
            redisUtil.set(redisKey, JSONUtil.toJsonStr(list));
            //设置未读数量为0个
            String rediske = TOTAL_KEY + userId;
            redisUtil.set(rediske, 0);

        }
        return Result.ok(list);
    }


}
