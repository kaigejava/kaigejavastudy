package com.kaigejava.system;

import cn.hutool.json.JSONUtil;
import com.kaigejava.commoneresult.Result;
import com.kaigejava.dto.LoginFormDTO;
import com.kaigejava.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2023/8/13 9:26
 */
@RestController
@RequestMapping("/login")
@Slf4j
public class LoginController {


    @RequestMapping("/userLogin")
    public Result userLogin( LoginFormDTO loginForm, HttpSession session) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(UUID.randomUUID().toString().replace("-", ""));
        userDTO.setNickName("啊哈哈");
        log.info("当前登录用户信息:{}", JSONUtil.toJsonStr(userDTO));
        return Result.ok(userDTO);
    }
    @GetMapping("/test")
    public String test(String msg){
        return "OK";
    }

}
