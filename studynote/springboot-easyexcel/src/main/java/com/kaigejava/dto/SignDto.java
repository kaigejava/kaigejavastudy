package com.kaigejava.dto;

/**
 * @description
 * @since 2022/5/7 16:18
 */
public class SignDto {
    private Long userId;
    private Integer dbNo;

    private Integer tableNo;

    private String sql;

}
