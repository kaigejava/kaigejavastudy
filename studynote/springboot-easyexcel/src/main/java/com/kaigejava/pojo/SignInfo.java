package com.kaigejava.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description
 * @since 2022/5/7 16:03
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SignInfo {

    //user_id,ctime ,type,subscription_time , expire_time,pline

    @ExcelProperty(value = "user_id")
    private Long userId;

    @ExcelProperty(value = "ctime")
    private Date ctime;


    @ExcelProperty(value = "type")
    private Integer type;


    @ExcelProperty(value = "subscription_time")
    private Date subscriptionTime;


    @ExcelProperty(value = "expire_time")
    private Date expireTime;


    @ExcelProperty(value = "pline")
    private Integer pline;
}
