package com.kaigejava.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author 凯哥Java
 * @description jedis连接池
 *
 * @company
 * @since 2022/11/24 19:34
 */
public class JedisFactory {

    private static final JedisPool jedisPool;

    static {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        //最大连接数
        poolConfig.setMaxTotal(8);
        //最大空闲链接
        poolConfig.setMaxIdle(8);
        //等待市场
        poolConfig.setMaxWaitMillis(1000);
        //最小空闲链接
        poolConfig.setMinIdle(0);
        jedisPool = new JedisPool(poolConfig,"127.0.0.1",6379);
    }

    public static Jedis getJedis(){
        return jedisPool.getResource();
    }

}
