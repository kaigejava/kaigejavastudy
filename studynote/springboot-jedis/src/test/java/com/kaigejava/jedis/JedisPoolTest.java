package com.kaigejava.jedis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 凯哥Java
 * @description
 * @company
 * @since 2022/11/24 19:38
 */
public class JedisPoolTest {

    private Jedis jedis;

    @BeforeEach
    public void initJedis(){
        jedis = JedisFactory.getJedis();
    }

    @Test
    public void testString(){
        String result = jedis.set("name","kaige");
        System.out.println("set Result"+result);
        String nameValue = jedis.get("name");
        System.out.println("v:"+nameValue);
    }


    @Test
    public void hashTest(){
        Map<String,String> value = new HashMap<>();
        value.put("id","1");
        value.put("name","hset1");
        value.put("age","23");
        Long result = jedis.hset("persion_1",value);
        System.out.println("set Result"+result);
        String age = jedis.hget("persion_1","age");
        System.out.println("age:"+age);
    }


}
