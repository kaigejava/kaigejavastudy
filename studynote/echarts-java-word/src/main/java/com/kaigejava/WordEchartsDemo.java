package com.kaigejava;

/**
 * @author 凯哥Java
 * @since 2023/5/24 13:04
 */
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;

import cn.hutool.json.JSONUtil;
import com.github.abel533.echarts.code.Trigger;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Line;
import org.apache.xmlbeans.XmlCursor;


public class WordEchartsDemo {
    public static void main(String[] args) throws Exception {
        // 生成Echarts Option
        String[] categories = {"周一", "周二", "周三", "周四", "周五", "周六", "周日"};
        Option option = createEchartsOption(categories);

        // 生成Echarts图片
        BufferedImage echartsImage = createEchartsImage(option);

        // 将图片插入Word文档
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(echartsImage, "png", baos);
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        insertImageToWord(bais, "D:\\files\\test\\wordTemplate.docx", "D:\\files\\test\\wordOutput.docx");
    }

    // 生成Echarts Option
    private static Option createEchartsOption(String[] categories) {
        GsonOption option = new GsonOption();
        option.title().text("某商场销售情况").subtext("纯属虚构");
        option.tooltip().trigger(Trigger.axis);
        option.legend().data("最高气温", "最低气温").left("center");
        option.xAxis(new CategoryAxis().data(categories));
        option.yAxis(new com.github.abel533.echarts.axis.ValueAxis());
        option.series(new Line().name("最高气温").data(11, 11, 15, 13, 12, 13, 10),
                new Line().name("最低气温").data(1, -2, 2, 5, 3, 2, 0));
        return option;
    }

    // 生成Echarts图片
    private static BufferedImage createEchartsImage(Option option) throws Exception {
        String optionDataJson = JSONUtil.toJsonStr(option);
        BufferedImage echartsImage = EChartsUtil.generateEChartConvertFileToImage(optionDataJson, "D:\\files\\test\\", EChartWord.getEChartJSPath());
        return echartsImage;
    }

    // 将图片插入Word文档
    private static void insertImageToWord(InputStream inputStream, String wordTemplatePath, String wordOutputPath)
            throws Exception {
        XWPFDocument doc = new XWPFDocument(new FileInputStream(wordTemplatePath));
        XmlCursor cursor = doc.getDocument().getBody().newCursor();
        XWPFParagraph paragraph = doc.createParagraph();
        XWPFRun run = paragraph.createRun();
        String blipId = doc.addPictureData(inputStream, org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_PNG);
        run.setText("");
        run.addPicture(new ByteArrayInputStream(blipId.getBytes()), org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_PNG, "echarts", Units.toEMU(500), Units.toEMU(300));
        //cursor.insertNewLine();
       // cursor.insertElemAfter("\n");

        FileOutputStream out = new FileOutputStream(wordOutputPath);
        doc.write(out);
        out.close();
    }
}
