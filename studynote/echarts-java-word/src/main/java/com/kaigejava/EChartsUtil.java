package com.kaigejava;

/**
 * @author 凯哥Java
 * @since 2023/5/24 14:42
 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.UUID;

import static com.kaigejava.EChartWord.getPhantomjsPath;

public class EChartsUtil {

    /**
     * 生成EChart图
     *
     * @param options      EChart初始化json
     * @param tmpPath      临时文件存放处
     * @param echartJsPath 第三方工具路径--也就是js的路径
     * @return
     */
    public static  String generateEChartConvertFilePath(String options, String tmpPath, String echartJsPath)  {
        return  generateEChart(options, tmpPath,echartJsPath);
    }
    /**
     * 生成EChart图
     *
     * @param options      EChart初始化json
     * @param tmpPath      临时文件存放处
     * @param echartJsPath 第三方工具路径--也就是js的路径
     * @return
     */
    public static  InputStream generateEChartConvert(String options, String tmpPath, String echartJsPath) throws IOException {
        String dataPath = generateEChart(options, tmpPath,echartJsPath);
        return new FileInputStream(new File(dataPath));
    }

        /**
         * 生成EChart图
         *
         * @param options      EChart初始化json
         * @param tmpPath      临时文件存放处
         * @param echartJsPath 第三方工具路径--也就是js的路径
         * @return
         */
    public  static BufferedImage  generateEChartConvertFileToImage(String options, String tmpPath, String echartJsPath) throws IOException {
        // 生成Echart的初始化json文件
        String dataPath = generateEChart(options, tmpPath,echartJsPath);
        BufferedImage image = ImageIO.read(new File(dataPath));
        return image;
    }
    /**
     * 生成EChart图
     *
     * @param options      EChart初始化json
     * @param tmpPath      临时文件存放处
     * @param echartJsPath 第三方工具路径--也就是js的路径
     * @return
     */
    private static String generateEChart(String options, String tmpPath, String echartJsPath) {
        // 生成Echart的初始化json文件
        String dataPath = writeFile(options, tmpPath);
        // 生成随机文件名
        String fileName = UUID.randomUUID().toString().substring(0, 8) + ".png";
        String path = tmpPath + fileName;
        try {
            // 文件路径（路径+文件名）
            File file = new File(path);
            // 文件不存在则创建文件，先创建目录
            if (!file.exists()) {
                File dir = new File(file.getParent());
                dir.mkdirs();
                file.createNewFile();
            }

            // 这里只能写绝对路径，因为要执行系统命令行
            String cmd = getPhantomjsPath() + " " +
                    echartJsPath +
                    " -infile " + dataPath +
                    " -outfile " + path;
            System.out.println("cmd:"+cmd);
            Process process = Runtime.getRuntime().exec(cmd);

            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = "";
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            input.close();

            // 删除生成的临时json文件
            File jsonFile = new File(dataPath);
            jsonFile.delete();
            return path;
        } catch (IOException e) {
            e.printStackTrace();
            return path;
        }
    }


    /**
     * 保存EChart临时json
     *
     * @param options echart初始化js
     * @param tmpPath 临时文件保存路径
     * @return 文件完整路径
     */
    private static String writeFile(String options, String tmpPath) {
        String dataPath = tmpPath + UUID.randomUUID().toString().substring(0, 8) + ".json";
        try {
            /* 写入Txt文件 */
            // 相对路径，如果没有则要建立一个新的output.txt文件
            File writeName = new File(dataPath);
            // 文件不存在则创建文件，先创建目录
            if (!writeName.exists()) {
                File dir = new File(writeName.getParent());
                dir.mkdirs();
                // 创建新文件
                writeName.createNewFile();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(writeName));
            out.write(options);
            // 把缓存区内容压入文件
            out.flush();
            // 最后记得关闭文件
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataPath;
    }
}

