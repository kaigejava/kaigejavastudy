package com.kaigejava;

import cn.hutool.json.JSONUtil;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.code.Magic;
import com.github.abel533.echarts.code.Tool;
import com.github.abel533.echarts.code.Trigger;
import com.github.abel533.echarts.code.X;
import com.github.abel533.echarts.data.LineData;
import com.github.abel533.echarts.data.PieData;
import com.github.abel533.echarts.feature.MagicType;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Funnel;
import com.github.abel533.echarts.series.Line;
import com.github.abel533.echarts.series.Pie;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author 凯哥Java
 * @description
 *所需要的软件包：
 * 链接: https://pan.baidu.com/s/1guTP-9RsZtSDFj1XFLzXqA?pwd=vgay 提取码: vgay
 * @since 2023/5/24 17:07
 */
public class Demo5 {
    public static void main(String[] args) throws Exception {
        XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun r = p.createRun();
        r.setText("图表展示");
        r.setColor("000000");
        r.setFontFamily("微软雅黑");
        r.setFontSize(18);

        String[] categories = {"周一", "周二", "周三", "周四", "周五", "周六", "周日"};
        //这里是折线图
        Option option = createEchartsOption(categories);
                insertImageToWord(doc,option);
        //饼状图
//        Option pieOption = getPieOption();
//        insertImageToWord(doc,pieOption);


    }

    /**
     * 获取饼状图的
     * @return
     */
    private static Option getPieOption() {
        GsonOption option = new GsonOption();
        //时间轴
        option.timeline().data("2013-01-01", "2013-02-01", "2013-03-01", "2013-04-01", "2013-05-01",
                new LineData("2013-06-01", "emptyStart6", 8), "2013-07-01", "2013-08-01", "2013-09-01", "2013-10-01",
                "2013-11-01", new LineData("2013-12-01", "star6", 8));
       // option.timeline().autoPlay(true);
        //timeline变态的地方在于多个Option
        Option basic = new Option();
        //// 大标题、小标题、位置
        basic.title().text("浏览器占比变化").subtext("纯属虚构").x("left");
        //提示工具,在轴上触发提示数据
        basic.tooltip().trigger(Trigger.item).formatter("{a} <br/>{b} : {c} ({d}%)");
        // 工具栏
        basic.toolbox().show(true).feature(Tool.mark, Tool.dataView, Tool.restore, Tool.saveAsImage, new MagicType(Magic.pie, Magic.funnel)
                .option(new MagicType.Option().funnel(
                        new Funnel().x("25%").width("50%").funnelAlign(X.left).max(1548))));

        // 图例
        basic.legend().data("Chrome", "Firefox", "Safari", "IE9+", "IE8-");


        int idx = 1;
        Pie pie = getPie(idx++).center("50%", "45%").radius("50%");
        pie.label().normal().show(true).formatter("{b}{c}({d}%)");
        basic.series(pie);
        //加入
        option.options(basic);
        //构造11个数据
        Option[] os = new Option[11];
        for (int i = 0; i < os.length; i++) {
            os[i] = new Option().series(getPie(idx++));
        }
        option.options(os);
        return option;


    }

    /**
     * 获取饼图数据
     *
     * @param idx
     * @return
     */
    public static Pie getPie(int idx) {
        return new Pie().name("浏览器（数据纯属虚构）").data(
                new PieData("Chrome", idx * 128 + 80),
                new PieData("Firefox", idx * 64 + 160),
                new PieData("Safari", idx * 32 + 320),
                new PieData("IE9+", idx * 16 + 640),
                new PieData("IE8-", idx * 8 + 1280));
    }
    // 生成Echarts Option
    private static Option createEchartsOption(String[] categories) {
        GsonOption option = new GsonOption();
        option.title().text("某商场销售情况").subtext("纯属虚构");
        option.tooltip().trigger(Trigger.axis);
        option.legend().data("最高气温", "最低气温").left("center");
        option.xAxis(new CategoryAxis().data(categories));
        option.yAxis(new com.github.abel533.echarts.axis.ValueAxis());
        option.series(new Line().name("最高气温").data(11, 11, 15, 13, 12, 13, 10),
                new Line().name("最低气温").data(1, -2, 2, 5, 3, 2, 0));
        return option;
    }

    public static void insertImageToWord(XWPFDocument doc,Option dataOption) throws Exception {
        FileOutputStream out = new FileOutputStream(EChartWord.eChartTempPath+"output.docx");
        XWPFParagraph p = doc.createParagraph();
        XWPFRun r = p.createRun();
        String optionDataJson = JSONUtil.toJsonStr(dataOption);
        System.out.println(optionDataJson);
        String imagePath = EChartsUtil.generateEChartConvertFilePath(optionDataJson, EChartWord.eChartTempPath, EChartWord.getEChartJSPath());
        int format;
        switch (imagePath.substring(imagePath.lastIndexOf(".") + 1).toLowerCase()) {
            case "png":
                format = XWPFDocument.PICTURE_TYPE_PNG;
                break;
            case "jpg":
            case "jpeg":
                format = XWPFDocument.PICTURE_TYPE_JPEG;
                break;
            case "bmp":
                format = XWPFDocument.PICTURE_TYPE_BMP;
                break;
            default:
                throw new Exception("不支持该图片格式");
        }
        r.addPicture(new FileInputStream(new File(imagePath)), format, imagePath, Units.toEMU(450), Units.toEMU(250));
        doc.write(out);
        out.close();
    }
}
