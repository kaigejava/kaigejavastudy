package com.kaigejava;

/**
 * @author 凯哥案卷
 * @description 后台生成EChart图片并插入Word测试类
 * @since 2023/5/24 16:12
 */
public class EChartWord {

    // ============== 这里要改成自己电脑对应的文件位置，服务器部署时可以通过环境变量等方式来动态改变它们的值 =================
    /**
     * echart-convert包的路径
     */

    /**
     * echart临时文件存储路径
     */
    public static  String eChartTempPath = "D:\\files\\test\\";
    /**
     * phantomjs命令路径
     */
    private static String OS = System.getProperty("os.name").toLowerCase();

    /**
     * 获取echartJs路径的
     * @return 全路径的echarts
     */
    public static String getEChartJSPath(){
       /* String echartsConvert1jsPath =   EChartWord.class.getClassLoader().getResource("javaecharts/echarts-convert/echarts-convert1.js").getPath();
        return echartsConvert1jsPath;*/
        return "D:\\BaiduNetdiskDownload\\服务器生成echart图片工具包\\echarts-convert\\echarts-convert1.js";

    }

    /**
     * 获取phantomjs的路径
     * @return js的全路径
     */
    public static String getPhantomjsPath(){
        if(isLinux()){
            String filePath = EChartWord.class.getClassLoader().getResource("javaecharts/phantomjs-2.1.1-linux-x86_64/bin/phantomjs").getPath();
            return filePath;
        }else if(isWindows()){
           /* String filePath = EChartWord.class.getClassLoader().getResource("javaecharts/phantomjs-2.1.1-windows/bin/phantomjs.exe").getPath();
            return filePath;*/
            return  "D:\\BaiduNetdiskDownload\\服务器生成echart图片工具包\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe";


        }else if(isMacOSX()){
            String filePath = EChartWord.class.getClassLoader().getResource("javaecharts/phantomjs-2.1.1-macosx/bin/phantomjs").getPath();
            return filePath;
        }
        return "";
    }


    public static boolean isLinux() {
        return OS.indexOf("linux") >= 0;
    }

    public static boolean isMacOSX() {
        return OS.indexOf("mac") >= 0 && OS.indexOf("os") > 0 && OS.indexOf("x") > 0;
    }
    public static boolean isWindows() {
        return OS.indexOf("windows") >= 0;
    }




}
