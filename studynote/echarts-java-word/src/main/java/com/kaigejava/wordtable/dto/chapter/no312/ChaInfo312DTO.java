package com.kaigejava.wordtable.dto.chapter.no312;

import lombok.Data;

import java.util.List;

/**
 * @author 凯哥Java
 * @description 第3.1.2章节对象
 * @company HDTD
 * @since 2023/5/29 17:25
 */
@Data
public class ChaInfo312DTO {

    /**
     * 3.1.2.1	周期年度数据统计
     */
    private DataDTO data3121;

    /**
     * 3.1.2.2 数据对象
     */
    private List<DataDTO> data3122s;


}
