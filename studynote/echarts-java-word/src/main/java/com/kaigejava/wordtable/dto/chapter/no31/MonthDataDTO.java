package com.kaigejava.wordtable.dto.chapter.no31;

import lombok.Data;

import java.util.List;

/**
 * @author 凯哥Java
 * @description 3.1.1.2章节下的数据对象
 * @company
 * @since 2023/5/28 22:07
 */

@Data
public class MonthDataDTO{
    /**
     * 3.1.1.2 每个月数据统计 - 需要循环
     * 比如二月数据统计、三月数据统计中的 二月、三月
     */
    private String monthText;


    /**
     * 1）	发电设备日发电量统计
     */
    private String dayStatistics;

    /**
     *  每台风机本月发电量
     *  19号风机设备本月最大日发电量为33190kWh，最小日发电量为252.5kWh，日平均发电量为11231.34kWh，其中日发电量高于平均值的天数有16天。
     */
    // private DocumentRenderData monthDataStatisticsEveryMonthInDay;
    private List<MonthDataStatisticsEveryMonthInDayDTO> monthDataStatisticsEveryMonthInDays;

    /**
     *  2）减排数据统计
     */
    private String reductionStatisticsTitleText;

    /**
     * 3.1.1.2 每个月数据统计之 2）减排数据统计
     * 本月场站监测部分累计总发电量为7,075,093.94kWh。已知每度电消耗301.5克标煤，减排828克二氧化碳，减少22毫克烟尘、101毫克二氧化硫以及152毫克氮氧化物。通过分析计算，本月场站被监测部分累计减排数据如下：
     * 	共节约标煤2133.14吨
     * 	减少二氧化碳排放5858.18吨
     * 	减少烟尘排放155.65千克
     * 	减少二氧化硫排放714.58千克
     * 	减少氮氧化物排放1075.41千克
     */
    // private DocumentRenderData monthDataReductionStatistics;
    private MonthDataReductionStatisticsDTO monthDataReductionStatistics;


    /**
     * 设备每月日发电量统计图片
     *
     */
    private String monthDayImagePath;
    /**
     *设备每月日发电量统计图片说明
     * 例如:图 4 设备4月份日发电量统计
     */
    private String monthDayImageTitle;
}
