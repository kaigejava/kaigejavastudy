
package com.kaigejava.wordtable.dto;

import com.deepoove.poi.data.DocumentRenderData;
import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/26 16:53
 */
@Data
public class ChaInfoDTO {

	private DocumentRenderData documentRenderData;

	private String title;


}
