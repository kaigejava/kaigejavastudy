package com.kaigejava.wordtable.dto.chapter.no312;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description 3.1.2==>(1)	设备等效利用小时数列表中数据对象
 * @company HDTD
 * @since 2023/5/29 17:26
 */
@Data
public class HoursTableDTO {
    /**
     * 序号
     */
    private Integer sequence;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 最大值
     */
    private String max;

    /**
     * 最小值
     */
    private String min;
}
