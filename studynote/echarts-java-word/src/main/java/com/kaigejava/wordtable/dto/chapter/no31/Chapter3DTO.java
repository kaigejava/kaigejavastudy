package com.kaigejava.wordtable.dto.chapter.no31;

import com.deepoove.poi.data.DocumentRenderData;
import lombok.Data;

import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/26 11:04
 */
@Data
public class Chapter3DTO {


    private String bfb;

    private String tjyx;

    /**
     * 3.1.1.1循环段落
     */
    private DocumentRenderData paragraphs;
    /**
     * 3.1.1.1图片
     */
    private String imagePath;

    /**
     * 3.1.1.1图片描述
     */
    private DocumentRenderData imageMessageDocument;

    /**
     * 从下面开始是 3.1.1.2 阅读发电数据统计相关的。这里其实也可以使用对象
     ***/
    private String monthDataStatisticsStr;
    /**
     * 数据信息
     */
    private List<MonthDataDTO> resources;


}





