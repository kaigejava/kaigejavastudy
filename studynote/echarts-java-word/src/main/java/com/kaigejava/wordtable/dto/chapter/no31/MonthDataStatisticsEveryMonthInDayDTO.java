package com.kaigejava.wordtable.dto.chapter.no31;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description 3.1.1.2章节下的数据对象下的每台风机本月发电量对象
 * 19号风机设备本月最大日发电量为33190kWh，最小日发电量为252.5kWh，日平均发电量为11231.34kWh，其中日发电量高于平均值的天数有16天。
 * @company
 * @since 2023/5/28 22:09
 */
@Data
public class MonthDataStatisticsEveryMonthInDayDTO {
    /**
     * 机器型号
     * 比如19号
     */
    private String modelNo;

    /**
     * 场站类型
     * 比如：风机
     */
    private String stationType;

    /**
     * 最大日发电量
     * 比如：33190kWh
     */
    private String dayMax;

    /**
     * 最小日发电量
     * 比如：252.5kWh
     */
    private String dayMin;

    /**
     * 日平均发电量
     * 比如：11231.34kWh
     */
    private String dayAvg;

    /**
     *
     * 其中日发电量高于平均值的天数有X天
     * 比如：16天
     */
    private String gtDayAvg;


}
