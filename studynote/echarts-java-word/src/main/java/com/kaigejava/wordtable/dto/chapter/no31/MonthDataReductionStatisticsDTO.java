package com.kaigejava.wordtable.dto.chapter.no31;


import lombok.Data;

/**
 * @author 凯哥Java
 * @since 2023/5/28 22:08
 * @description 3.1.1.2 每个月数据统计之 2）减排数据统计
 * 本月场站监测部分累计总发电量为7,075,093.94kWh。已知每度电消耗301.5克标煤，减排828克二氧化碳，减少22毫克烟尘、101毫克二氧化硫以及152毫克氮氧化物。通过分析计算，本月场站被监测部分累计减排数据如下：
 * 	共节约标煤2133.14吨
 * 	减少二氧化碳排放5858.18吨
 * 	减少烟尘排放155.65千克
 * 	减少二氧化硫排放714.58千克
 * 	减少氮氧化物排放1075.41千克
 */
@Data
public class MonthDataReductionStatisticsDTO {
    /**
     * 总发电量
     * 比如：7,075,093.94kWh
     */
    private String totalMonth;

    /**
     * 每度点消耗标煤
     * 比如：301.5
     */
    private String consumeCoal;

    /**
     * 二氧化碳
     * 比如：828
     */
    private String co2;

    /**
     * 烟尘
     * 比如：22
     */
    private String yc;

    /**
     * 二氧化硫
     * 比如：101
     */
    private String cox;
    /**
     * 氮氧化物
     * 比如：152
     */
    private String nOr;


    /**
     * 共节约标煤数量
     * 比如：2133.14
     */
    private String totalConsumeCoal;

    /**
     * 共减少二氧化碳数量
     * 比如：5858.18
     */
    private String totalCo2;


    /**
     * 共烟尘
     * 比如：22
     */
    private String totalYc;

    /**
     * 共二氧化硫
     * 比如：101
     */
    private String totalCox;
    /**
     * 共氮氧化物
     * 比如：152
     */
    private String totalNOr;



}
