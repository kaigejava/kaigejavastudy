package com.kaigejava.wordtable.dto;

import com.deepoove.poi.config.Configure;
import lombok.Data;

import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/25 19:00
 */
@Data
public class Tb1DataInfoDTO {


    private List<Tb1DataDTO> dataList;
    //dataMap.put("tb1Total","109491.35MWh");
    private String tb1Total;
    // dataMap.put("tb1MaxMothTotal","13378.05 MWh");
    private String tb1MaxMothTotal;
    //  dataMap.put("talMaxMoth","1");
    private String talMaxMoth;
    //  dataMap.put("tb1MaxMinTotal","2017.56 MWh");
    private String tb1MaxMinTotal;
    // dataMap.put("talMinMoth","7");
    private String talMinMoth;
    // dataMap.put("tb1Avg","9124.28MWh");
    private String tb1Avg;

    // dataMap.put("tb1Max","109491.35MWh");
    private String tb1Max;
    //dataMap.put("tb1Avg2","2.35MWh");
    private String tb1Avg2;
    // dataMap.put("table1Config",table1Config);
    private Configure table1Config;
}
