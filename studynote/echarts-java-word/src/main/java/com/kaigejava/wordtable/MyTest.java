package com.kaigejava.wordtable;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.data.*;
import com.deepoove.poi.data.style.ParagraphStyle;
import com.deepoove.poi.plugin.table.LoopRowTableRenderPolicy;
import com.deepoove.poi.policy.DocumentRenderPolicy;
import com.kaigejava.EChartsUtil;
import com.kaigejava.wordtable.dto.Tb1DataDTO;
import com.kaigejava.wordtable.dto.Tb1DataInfoDTO;
import com.kaigejava.wordtable.dto.WordParamDTO;
import com.kaigejava.wordtable.dto.chapter.no31.Chapter3DTO;
import com.kaigejava.wordtable.dto.chapter.no31.MonthDataDTO;
import com.kaigejava.wordtable.dto.chapter.no31.MonthDataReductionStatisticsDTO;
import com.kaigejava.wordtable.dto.chapter.no31.MonthDataStatisticsEveryMonthInDayDTO;
import org.apache.commons.compress.utils.Lists;
import org.apache.poi.xwpf.usermodel.*;

import java.io.File;
import java.util.*;

import static com.kaigejava.EChartWord.*;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/25 11:30
 */
public class MyTest {

    public static void main(String[] args) throws Exception {
        String imagePath = "";
        try {
            String base_path = "D:\\files\\test\\";
            String tempPath = base_path + "temp1.docx";
            String savePATH = base_path + "myTest-out.docx";
            WordParamDTO wordParamDTO = new WordParamDTO();
            wordParamDTO.setCompanyName("限公司");
            wordParamDTO.setProjectName("风电场");
            wordParamDTO.setStartTime("2022年2月");
            wordParamDTO.setEndTime("2023年1月");
            wordParamDTO.setProjectAddr("xx市");
            wordParamDTO.setProjectType("风机");
            wordParamDTO.setTotal("100MW");
            wordParamDTO.setJcNum("15");
            wordParamDTO.setJcTotal("33.1MW");
            String optionStr = "{\"title\":{\"text\":\"某商场销售情况\",\"subtext\":\"纯属虚构\"},\"tooltip\":{\"trigger\":\"axis\"},\"legend\":{\"data\":[\"最高气温\",\"最低气温\"],\"left\":\"center\"},\"xAxis\":[{\"type\":\"category\",\"data\":[\"周一\",\"周二\",\"周三\",\"周四\",\"周五\",\"周六\",\"周日\"]}],\"yAxis\":[{\"type\":\"value\"}],\"series\":[{\"name\":\"最高气温\",\"type\":\"line\",\"data\":[11,11,15,13,12,13,10]},{\"name\":\"最低气温\",\"type\":\"line\",\"data\":[1,-2,2,5,3,2,0]}]}";
            imagePath = EChartsUtil.generateEChartConvertFilePath(optionStr, eChartTempPath, getEChartJSPath());
            System.out.println("图片地址："+imagePath );
            wordParamDTO.setImage1(imagePath);
            //处理表格的
            /*表格*/
            /*表格*/
            addTable(wordParamDTO);

            //添加合并单元格
            addMergeCellRule(wordParamDTO);
            //添加第3章节内容
            add3(wordParamDTO);

            Configure table1Config = Configure.builder().bind("tb1DataInfoDTO.dataList", new LoopRowTableRenderPolicy())
                    .bind("chapter3.paragraphs", new DocumentRenderPolicy())
                    .bind("chapter3.imageMessageDocument", new DocumentRenderPolicy())
                    /*.bind("chapter3.monthDataStatisticsEveryMonth", new DocumentRenderPolicy())
                    .bind("chapter3.monthDataStatisticsEveryMonthInDay", new DocumentRenderPolicy())
                    .bind("chapter3.monthDataStatisticsEveryMonthInDay", new DocumentRenderPolicy())*/
                    .build();
            //TODO  XWPFTemplate.compile(tempPath, table1Config);必须写在config配置之后
            XWPFTemplate xwpfTemplate = XWPFTemplate.compile(tempPath, table1Config);
            xwpfTemplate.render(wordParamDTO).writeToFile(savePATH);


            System.out.println("生成了");
        } finally {
            File f = new File(imagePath);
            f.delete();
        }
    }

    private static void add3(WordParamDTO wordParamDTO) {
        Chapter3DTO chapter3 = new Chapter3DTO();
        chapter3.setBfb("33.1%");
        chapter3.setTjyx("风速气象条件、设备运行状态");
        String[] texts = new String[]{"19号风机设备本年最大月发电量为688,985kWh，最小月发电量为85,852.5kWh，月平均发电量为431,624kWh，其中月发电量高于均值的月份有6个。"
                , "20号风机设备本年最大月发电量为757,883.5kWh，最小月发电量为94,437.75kWh，月平均发电量为474,786.35kWh，其中月发电量高于均值的月份有7个。"
                , "21号风机设备本年最大月发电量为1040,900.8kWh，最小月发电量为170,985kWh，月平均发电量为719,220.15kWh，其中月发电量高于均值的月份有7个。"
                , "22号风机设备本年最大月发电量为1059,500kWh，最小月发电量为145,905kWh，月平均发电量为704,473.33kWh，其中月发电量高于均值的月份有5个。"
                , "25号风机设备本年最大月发电量为847600kWh，最小月发电量为155,805kWh，月平均发电量为588454.46kWh，其中月发电量高于均值的月份有6个。"};
        // 循环创建多个段落对象，并为每个段落添加文本和样式
        DocumentRenderData document = Documents.of().create();
        for (int i = 0; i < 5; i++) {
            // 创建段落对象
            ParagraphRenderData paragraph = Paragraphs.of().addText(texts[i]).create();
            ParagraphStyle paragraphStyle = ParagraphStyle.builder().withSpacing(1.5f)
                    .withIndentFirstLineChars(2f).build();
            paragraph.setParagraphStyle(paragraphStyle);
            document.addParagraph(paragraph);

        }
        DocumentRenderData imageMessageDocument = Documents.of().create();
        //设置图片下面文案
        ParagraphRenderData imageParagraph = Paragraphs.of().addText("图 1 设备年度发电量曲线").create();
        ParagraphStyle paragraphStyle = ParagraphStyle.builder().withSpacing(1.5f)
                //ParagraphAlignment
                .withIndentFirstLineChars(2f).withAlign(ParagraphAlignment.CENTER).build();
        imageParagraph.setParagraphStyle(paragraphStyle);
        imageMessageDocument.addParagraph(imageParagraph);
        chapter3.setParagraphs(document);
        //设置图片
        chapter3.setImageMessageDocument(imageMessageDocument);
        //设置图片
        String optionStr = "{\"title\":{\"text\":\"设备年度发电量曲线\",\"subtext\":\"纯属虚构\"},\"tooltip\":{\"trigger\":\"axis\"},\"legend\":{\"data\":[\"最高气温\",\"最低气温\"],\"left\":\"center\"},\"xAxis\":[{\"type\":\"category\",\"data\":[\"周一\",\"周二\",\"周三\",\"周四\",\"周五\",\"周六\",\"周日\"]}],\"yAxis\":[{\"type\":\"value\"}],\"series\":[{\"name\":\"最高气温\",\"type\":\"line\",\"data\":[11,11,15,13,12,13,10]},{\"name\":\"最低气温\",\"type\":\"line\",\"data\":[1,-2,2,5,3,2,0]}]}";
        String imagePath = EChartsUtil.generateEChartConvertFilePath(optionStr, eChartTempPath, getEChartJSPath());
        System.out.println("图片地址22："+imagePath );

        chapter3.setImagePath(imagePath);

        //3.1.1.2 阅读发电数据统计 标题4
        //下面开始循环每个月的
        //(1) 二月数据统计  标题5
        //1)发电设备日发电量统计。标题6
        //下面开始每个机器。每日发电数据统计情况
        //图片。标题6统计
        //2)减排数据统计

        /*ParagraphRenderData monthDataStatisticsParagraph =  Paragraphs.of().addText("3.1.1.2\t 月度发电数据统计").create();
        //标题4：字体：黑体，小五或六号加粗 行距：多倍行距 1.75 字行（或者在段落设置中直接选择“标题4”可以自动应用默认行距） 段落间距：段前12磅，段后6磅，与下段同页，段中不分页，4级
        monthDataStatisticsParagraph.setParagraphStyle(paragraphStyle);*/
        chapter3.setMonthDataStatisticsStr("每-月度发电数据统计");


        List<String> monthList = Lists.newArrayList();
        monthList.add("二月");
        monthList.add("三月");
        monthList.add("四月");
        monthList.add("五月");
        monthList.add("十二月");

        List<String> modelNoList = Lists.newArrayList();
        modelNoList.add("19号");
        modelNoList.add("20号");
        modelNoList.add("21号");
        modelNoList.add("22号");
        modelNoList.add("23号");
        modelNoList.add("24号");


        List<String> dayMaxList = Lists.newArrayList();
        dayMaxList.add("33190kWh");
        dayMaxList.add("36509kWh");
        dayMaxList.add("47300kWh");
        dayMaxList.add("46372.5kWh");
        dayMaxList.add("45105kWh");
        dayMaxList.add("37098kWh");

        List<String> dayMinList = Lists.newArrayList();
        dayMinList.add("252.5kWh");
        dayMinList.add("278kWh");
        dayMinList.add("1285kWh");
        dayMinList.add("1260kWh");
        dayMinList.add("1008kWh");
        dayMinList.add("199kWh");


        List<String> dayAvgList = Lists.newArrayList();
        dayAvgList.add("11231.34kWh");
        dayAvgList.add("12354kWh");
        dayAvgList.add("20438kWh");
        dayAvgList.add("20037.14kWh");
        dayAvgList.add("20037kWh");
        dayAvgList.add("16030kWh");


        List<String> gtDayAvgList = Lists.newArrayList();
        gtDayAvgList.add("16");
        gtDayAvgList.add("12");
        gtDayAvgList.add("16");
        gtDayAvgList.add("10");
        gtDayAvgList.add("13");
        gtDayAvgList.add("12");


        String stationType = "风机";
        String dayStatistics = "1）\t发电设备日发电量统计";
        String reductionStatisticsTitleText = "2）减排数据统计";
        /**
         * 本月场站监测部分累计总发电量为7,075,093.94kWh。已知每度电消耗301.5克标煤，减排828克二氧化碳，减少22毫克烟尘、101毫克二氧化硫以及152毫克氮氧化物。通过分析计算，本月场站被监测部分累计减排数据如下：
         * 	共节约标煤2133.14吨
         * 	减少二氧化碳排放5858.18吨
         * 	减少烟尘排放155.65千克
         * 	减少二氧化硫排放714.58千克
         * 	减少氮氧化物排放1075.41千克
         */
        String totalMonth = "7,075,093.94kWh";
        String consumeCoal = "301.5";
        String co2 = "828";
        String yc = "22";
        String cox = "101";
        String nOr = "152";
        String totalConsumeCoal = "2133.14";
        String totalCo2 = "5858.18";
        String totalYc = "155.65";
        String totalCox = "714.58";
        String totalNor = "1075.41";

        int imageNo= 3;
        List<MonthDataDTO> resources = Lists.newArrayList();
        for (int i = 0; i < monthList.size(); i++) {
            MonthDataDTO monthDataDTO = new MonthDataDTO();
            monthDataDTO.setMonthText(monthList.get(i));
            monthDataDTO.setDayStatistics(dayStatistics);
            //每台风机情况
            List<MonthDataStatisticsEveryMonthInDayDTO> monthDataStatisticsEveryMonthInDays = Lists.newArrayList();
            for (int j = 0; j < modelNoList.size(); j++) {
                MonthDataStatisticsEveryMonthInDayDTO monthDataStatisticsEveryMonthInDayDTO = new MonthDataStatisticsEveryMonthInDayDTO();
                monthDataStatisticsEveryMonthInDayDTO.setModelNo(modelNoList.get(j));
                monthDataStatisticsEveryMonthInDayDTO.setStationType(stationType);
                monthDataStatisticsEveryMonthInDayDTO.setDayMax(dayMaxList.get(j));
                monthDataStatisticsEveryMonthInDayDTO.setDayMin(dayMinList.get(j));
                monthDataStatisticsEveryMonthInDayDTO.setGtDayAvg(gtDayAvgList.get(j));

                monthDataStatisticsEveryMonthInDays.add(monthDataStatisticsEveryMonthInDayDTO);
            }
            //设置每台风机情况
            monthDataDTO.setMonthDataStatisticsEveryMonthInDays(monthDataStatisticsEveryMonthInDays);
            //2)减排标题
            monthDataDTO.setReductionStatisticsTitleText(reductionStatisticsTitleText);

            //减排具体信息
            MonthDataReductionStatisticsDTO monthDataReductionStatistics = new MonthDataReductionStatisticsDTO();
            monthDataReductionStatistics.setTotalMonth(totalMonth);
            monthDataReductionStatistics.setCo2(co2);
            monthDataReductionStatistics.setYc(yc);
            monthDataReductionStatistics.setConsumeCoal(consumeCoal);
            monthDataReductionStatistics.setCox(cox);
            monthDataReductionStatistics.setNOr(nOr);
            monthDataReductionStatistics.setTotalConsumeCoal(totalConsumeCoal);
            monthDataReductionStatistics.setTotalCo2(totalCo2);
            monthDataReductionStatistics.setTotalYc(totalYc);
            monthDataReductionStatistics.setTotalCox(totalCox);
            monthDataReductionStatistics.setTotalNOr(totalNor);

            //设置具体减排信息
            monthDataDTO.setMonthDataReductionStatistics(monthDataReductionStatistics);

            //设置图片
            String monthDayImageOptionStr = "{\"title\":{\"text\":\"设备"+monthList.get(i)+"份日发电量统计\",\"subtext\":\"纯属虚构\"},\"tooltip\":{\"trigger\":\"axis\"},\"legend\":{\"data\":[\"最高气温\",\"最低气温\"],\"left\":\"center\"},\"xAxis\":[{\"type\":\"category\",\"data\":[\"周一\",\"周二\",\"周三\",\"周四\",\"周五\",\"周六\",\"周日\"]}],\"yAxis\":[{\"type\":\"value\"}],\"series\":[{\"name\":\"最高气温\",\"type\":\"line\",\"data\":[11,11,15,13,12,13,10]},{\"name\":\"最低气温\",\"type\":\"line\",\"data\":[1,-2,2,5,3,2,0]}]}";
            String monthDayImagePath = EChartsUtil.generateEChartConvertFilePath(monthDayImageOptionStr, eChartTempPath, getEChartJSPath());
            monthDataDTO.setMonthDayImagePath(monthDayImagePath);
            imageNo+=1;
            monthDataDTO.setMonthDayImageTitle("图 "+imageNo+" 设备"+monthList.get(i)+"份日发电量统计");
            resources.add(monthDataDTO);
        }

        chapter3.setResources(resources);
        wordParamDTO.setChapter3(chapter3);


    }

    /**
     * 合并单元格
     *
     * @param wordParamDTO
     */
    private static void addMergeCellRule(WordParamDTO wordParamDTO) {
        Tb1DataInfoDTO data = new Tb1DataInfoDTO();
        data.setTb1Total("109491.35MWh");
        //这里是数据
        List<Tb1DataDTO> dataList = new ArrayList<>();
        String[] yearMoth = new String[]{"22年2月", "22年4月", "22年5月", "22年6月", "22年7月", "22年8月", "22年9月", "22年10月", "22年11月", "22年12月", "23年1月"};
        int monthTotalInt = 0;
        int dayMaxInt = 1;
        int dayMinInt = 2;
        int dayAvgInt = 3;
        for (String s : yearMoth) {
            Tb1DataDTO dto = new Tb1DataDTO();
            dto.setYearMoth(s);
            monthTotalInt += 10;
            dayMaxInt += 2;
            dayMinInt += 3;
            dayAvgInt += 1;
            dto.setMonthTotal(monthTotalInt + "");
            dto.setDayMax(dayMaxInt + "");
            dto.setDayMin(dayMinInt + "");
            dto.setDayAvg(dayAvgInt + "");
            dataList.add(dto);
        }
        data.setDataList(dataList);

        data.setTb1MaxMothTotal("13378.05 MWh");
        data.setTalMaxMoth("1");
        data.setTb1MaxMinTotal("2017.56 MWh");
        data.setTalMinMoth("7");
        data.setTb1Avg("9124.28MWh");
        data.setTb1Max("109491.35MWh");
        data.setTb1Avg2("2.35MWh");
        LoopRowTableRenderPolicy hackLoopTableRenderPolicy = new LoopRowTableRenderPolicy();
        Configure table1Config = Configure.builder().bind("tb1DataInfoDTO.dataList", hackLoopTableRenderPolicy)
                .build();
        data.setTable1Config(table1Config);
        wordParamDTO.setTb1DataInfoDTO(data);


    }

    /**
     * 添加表格
     *
     * @param wordParamDTO
     */
    private static void addTable(WordParamDTO wordParamDTO) {
        RowRenderData row0 = Rows.of("序号", "设备名称", "装机容量").textColor("FFFFFF")
                .bgColor("4472C4").center().create();

        List<RowRenderData> list = Lists.newArrayList();
        list.add(row0);
        for (int i = 0; i < 10; i++) {
            RowRenderData row1 = Rows.create((i + 1) + "", (i + 1) + "号风机", (i + 3) + "MW");
            list.add(row1);
        }
        RowRenderData[] valueList = list.toArray(new RowRenderData[list.size()]);
        wordParamDTO.setTableTest(Tables.create(valueList));

    }


}
