package com.kaigejava.wordtable.dto;

import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/25 18:21
 */
@Data
public class Tb1DataDTO {

    private String yearMoth;
    private String monthTotal;
    private String dayMax;
    private String dayMin;
    private String dayAvg;

}
