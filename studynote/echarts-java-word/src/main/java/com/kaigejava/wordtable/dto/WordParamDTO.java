package com.kaigejava.wordtable.dto;

import com.deepoove.poi.data.*;
import com.kaigejava.wordtable.dto.chapter.no31.Chapter3DTO;
import lombok.Data;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/25 19:08
 */
@Data
public class WordParamDTO {
    //dataMap.put("companyName","限公司");
    private String companyName;
    // dataMap.put("projectName","风电场");
    private String projectName;
    // dataMap.put("startTime","2022年2月");
    private String startTime;
    //  dataMap.put("endTime","2023年1月");
    private String endTime;
    //  dataMap.put("projectAddr","xx市");
    private String projectAddr;
    //  dataMap.put("projectType","风机");
    private String projectType;
    //  dataMap.put("total","100MW");
    private String total;
    //  dataMap.put("jcNum","15");
    private String jcNum;
    //        dataMap.put("jcTotal","33.1MW");
    private String jcTotal;

    // dataMap.put("picture1",   Pictures.ofLocal(imagePath).size(120, 120).create());//插入图片
    //     dataMap.put("image1", imagePath);            // 设置图片宽高
    private PictureRenderData pricture1;
    private String image1;
    //           dataMap.put("tableTest", Tables.create(valueList));
    private TableRenderData tableTest;

    private Tb1DataInfoDTO tb1DataInfoDTO;

    private Chapter3DTO chapter3;


}
