package com.kaigejava.wordtable;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/25 10:56
 */
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class WordTableExample {

    public static void main(String[] args) throws IOException {
        // 创建文档对象
        XWPFDocument document = new XWPFDocument();
        // 创建表格对象
        XWPFTable table = document.createTable(11, 3);
        // 设置表格位置
        //table.setWidth("100%");
        //table.setRowsHeight(300);
        table.setRowBandSize(300);
        // 设置单元格内容
        table.getRow(0).getCell(0).setText("序号");
        table.getRow(0).getCell(1).setText("设备名称");
        table.getRow(0).getCell(2).setText("装机容量");
        // 创建表格头部第二种方法
        /*XWPFTableRow header = table.getRow(0);
        header.getCell(0).setText("Name");
        header.addNewTableCell().setText("Gender");
        header.addNewTableCell().setText("Age");*/
        for (int i = 1; i < 10; i++) {
            table.getRow(i).getCell(0).setText(i+"");
            table.getRow(i).getCell(1).setText(i+"号风机");
            table.getRow(i).getCell(2).setText(i+3+"MW");
        }

        // 合并单元格
       /* table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.RESTART);
        table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
        table.getRow(0).getCell(2).getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
        table.getRow(0).getCell(3).getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);*/
        // 添加边框
        for (XWPFTableRow row : table.getRows()) {
            for (XWPFTableCell cell : row.getTableCells()) {
                cell.getCTTc().addNewTcPr().addNewTcBorders().addNewBottom().setVal(STBorder.SINGLE);
                cell.getCTTc().addNewTcPr().addNewTcBorders().addNewTop().setVal(STBorder.SINGLE);
                cell.getCTTc().addNewTcPr().addNewTcBorders().addNewRight().setVal(STBorder.SINGLE);
                cell.getCTTc().addNewTcPr().addNewTcBorders().addNewLeft().setVal(STBorder.SINGLE);

            }
        }
        // 写入文件
        FileOutputStream outputStream = new FileOutputStream(new File("D:\\files\\test\\d2.docx"));
        document.write(outputStream);
        outputStream.close();
    }
}
