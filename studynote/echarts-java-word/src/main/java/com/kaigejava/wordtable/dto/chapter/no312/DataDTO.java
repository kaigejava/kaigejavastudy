package com.kaigejava.wordtable.dto.chapter.no312;

import lombok.Data;

import java.util.List;

/**
 * @author 凯哥Java
 * @description
 * @company HDTD
 * @since 2023/5/29 17:37
 */
@Data
public class DataDTO {
    /**
     * 3.1.2==>(1)	设备等效利用小时数列表中数据对象
     */
    private List<HoursTableDTO> hoursTableDTOS;

    /**
     * (2)	设备月等效利用小时数曲线图
     */
    private String hoursImagePath;

    /**
     * 图 14 设备月等效利用小时曲线
     */
    private String hoursImageMessage;

    /**
     * 月份，在3.1.2.2章节需要使用的
     */
    private String month;
}
